import numpy as np

def _get_smooth_score(raw_score):
    
    """
    finds trajectories which leave and re-enter the home
    state and re-score them with the home state's score.
    
    NOTE: this function only handle cases where the trajectory
    does not pass through the boundary of another state before
    returning to the home-state.
    """
    
    smoothed_score = np.copy(raw_score)
    start_here = 0
    flag_movement = False
    
    for position, score in enumerate(raw_score):
        # tells the base for loop where to start looking. 
        if position < start_here:
            continue
        elif score == -1:
            continue
        elif score != -1:
            home_score = score
            restof_score = raw_score[position:]
            n=0
            for i, x in enumerate(restof_score):
                global_position = position + i
                if x == -1:
                    flag_movement = True
                    n=n+1
                elif x != home_score:
                    # possibly a genuine state transition,
                    # but also possible the trajectory is not
                    # static and will move elsewhere.
                    start_here = global_position
                    flag_movement = False
                    break
                else:
                    if flag_movement == False:
                        # has not yet exited home state
                        continue
                    else:
                        # case of re-entry into home state
                        start_here = global_position
                        flag_movement = False
                        start, stop = global_position-n, global_position
                        smoothed_score[start:stop] = home_score
                        break     
    
    return smoothed_score

def _get_y_bout(smooth_score, poe):
    
    """
    """
    
    home_score = smooth_score[poe]
    remaining_score = smooth_score[poe:]
    bout = 0
    for score in remaining_score:
        if score == home_score:
            bout = bout + 1
        else:
            return bout

def _find_connecting_spectral_trajectories(smooth_score,
                                           max_time_travel=60,
                                           min_time_static=15):
    
    """
    """
    
    # pointer tells the for loop where to pick up after yield is called.
    pointer = 0
    
    for position, score in enumerate(smooth_score):
        if position < pointer:
            continue
        elif score == -1:
            continue
        else:
            x_entry = position
            x_score = score
            remaining_score = smooth_score[x_entry:]
            
            time_travel = 0
            x_bout = 0
            for _position, _score in enumerate(remaining_score):
                global_position = position + _position
                
                if _score != x_score:
                    if _score == -1:
                        # some amount of time in travel.
                        time_travel = time_travel + 1
                        continue
                        
                    else:
                        # movement to a different state.
                        y_entry = global_position
                        y_score = _score
                        y_bout = _get_y_bout(smooth_score, y_entry)
                        x_exit = y_entry - time_travel
                        
                        # very important this comes before the break statement.
                        pointer = global_position
                        
                        # test here for validity.
                        try:
                            assert x_bout >= min_time_static
                            assert y_bout >= min_time_static
                            assert time_travel <= max_time_travel
                            
                        except AssertionError:
                            break
                        
                        # if it passes the validity checks, yield the traj.
                        yield x_score, y_score, time_travel, x_bout, y_bout, (x_exit - 1, y_entry + 1)
                        break
                
                else:
                    x_bout = x_bout + 1
                    continue
    
# (below) - works, but not sure if it is appropriate to implement.
#def _test_trajectory_homebound(raw_score, startof_trajectory):
#    
#    """
#    given the beginning of a trajectory which connects two states,
#    tests whether the trajectory returns to the homestate, or if
#    the transition is valid (at least one-way). if true, returns
#    the range of the homebound trajectory.
#    """
#    # must be 1 if beginnning of traj. part of home-state,
#    # i.e. score of first position must be -1.
#    start_here = 1
#    home_score = raw_score[startof_trajectory]
#    epoch = raw_score[startof_trajectory:]
#    assert epoch[0] == -1 # will raise error if not True
#    for i, x in enumerate(epoch):
#        global_position = startof_trajectory + i
#        if i < start_here:
#            continue
#        elif x == -1:
#            # moving through iss
#            continue
#        elif x != home_score: # transition to other state
#            # test if transition valid.
#            flag_static = _test_tail_of_trajectory_static(raw_score,
#                                                        global_position,
#                                                        mode='dest')
#            if flag_static == True:
#                # potentially a genuine transition.
#                return False
#            else:
#                # invalid transition, look for next boudnary crossing.
#                next_exit = _get_next_state_exit(raw_score,
#                                                 global_position)
#                start_here = i + next_exit
#                #pdb.set_trace()
#                continue
#        else: # x-score and home-score equal, return to home-state
#            # returns start, and stop indeces for slice of data
#            # that should be rescored with the home-state score.
#            return True, (startof_trajectory, global_position-1)
#        
#    return False