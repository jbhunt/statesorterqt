from __future__ import division
from matplotlib.cm import get_cmap
from matplotlib.colors import LinearSegmentedColormap, LogNorm, from_levels_and_colors
from matplotlib import gridspec
from matplotlib import pylab as plt
import numpy as np
from statesorterqt.spectr import _find_connecting_spectral_trajectories

def _plot_spectral_trajectory(ax, X_slice, id1, id2, cmap='native', lw=1):
    
    """
    """
    
    xy = X_slice.reshape(-1,1,2)
    line_segments = np.hstack([xy[:-1], xy[1:]])
    n_segments = len(line_segments)
    
    if cmap == 'native':
        native_colors = ['C0', 'C1', 'C2']
        c_home, c_dest = native_colors[id1], native_colors[id2]
        _cmap = LinearSegmentedColormap.from_list('custom_cmap', [c_home, c_dest], n_segments) 
        cmaplist = [_cmap(i) for i in range(_cmap.N)]
    
    else:
        _cmap = get_cmap(cmap, n_segments)
        cmaplist = [_cmap(i) for i in range(_cmap.N)]
        
    for i, seg in enumerate(line_segments):
        if ax == None:
            ax.plot(seg[:,0], seg[:,1], lw=lw, color=cmaplist[i])
        else:
            ax.plot(seg[:,0], seg[:,1], lw=lw, color=cmaplist[i])
            
def _plot_spectral_trajectories(ax, X, id1, id2,
                                smooth_score,
                                boundary_vertices,
                                max_time_transit=60,
                                min_time_static=15,
                                lw=1,
                                cmap='native'):
    
    """
    plots the spectral trajectories for all of the valid state
    transitions between state-x and state-y (as specified by id1
    and id2).
    """
    
    if cmap == 'native':
        native_colors = ['C0', 'C1', 'C2']
        c_home, c_dest = native_colors[id1], native_colors[id2]
        a = 0.6
        
    else:
        _cmap = get_cmap(cmap, 2)
        c_home, c_dest = _cmap(0), _cmap(1)
        a = 0.4
    
    trajs = _find_connecting_spectral_trajectories(smooth_score,
                                                       max_time_transit,
                                                       min_time_static)
    for traj in trajs:
        x_score, y_score = traj[:2]
        traj_range = traj[-1]
        
        if (x_score == id1) & (y_score == id2):    
            start, stop = traj_range
            X_slice = X[start:stop]
            _plot_spectral_trajectory(ax, X_slice,
                                      id1, id2,
                                      cmap=cmap)
    
    for i, vset in enumerate(boundary_vertices):
        ax.plot(vset[:,0], vset[:,1], color='k', lw=2)
        if i == id1:
            ax.fill(vset[:,0], vset[:,1], color=c_home, alpha=a)
        if i == id2:
            ax.fill(vset[:,0], vset[:,1], color=c_dest, alpha=a)
            
def _plot_percent_time_in_each_state_over_time(ax,
                                               cluster_masks,
                                               bin_width=3,
                                               fid=None):
    
    """
    plots the percent of time spent in each cluster (state) for
    each bin of n-hours (n as specified by bin_width kwarg).
    """
    
    n_secs = cluster_masks[0].shape[0]
    
    n_hours = n_secs / 3600
    interval_insecs = bin_width * 3600
    total_percentages = []
    for i, mask in enumerate(cluster_masks):
        y = []
        for ii in range(0, n_secs, interval_insecs):
            epoch = mask[ii:ii+interval_insecs]
            time_in_epoch = float(len(epoch))
            n = np.sum(epoch)
            y.append(n/time_in_epoch)
           
        x = range(len(y))
        ax.plot(x, y, 's-', markerfacecolor='w',
                  markeredgecolor='k',
                  label='cluster-{}'.format(i), lw=2)
        total_percentages.append(y)
        
    total_percentages = np.array(total_percentages)
    total_percentages = total_percentages.sum(0)
    ax.plot(x, total_percentages, 's-', color='k',
              markerfacecolor='w', markeredgecolor='k',
              label='total', lw=2)
    
    #if fid != None:
    #    idx_of_7am = _get_index_for_7am_day_of_recording(fid)
    #    start_pos = idx_of_7am / 3600 / bin_width
    #    stop_pos = start_pos + (12 / bin_width)
    #    ax.fill_betweenx([0,1], start_pos, stop_pos,
    #                       color='k', alpha=0.2, lw=0)
    #    
    #    _7am_next_day = start_pos + 24 / bin_width
    #    next_start_pos, next_stop_pos = _7am_next_day, _7am_next_day + (12 / bin_width)
    #    ax.fill_betweenx([0,1], next_start_pos, next_stop_pos,
    #                       color='k', alpha=0.2, lw=0)
    
    ax.set_xlim([min(x)-0.5, max(x)+0.5])
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.legend(loc=1)

def _plot_hypnogram(ax, smooth_score, n=3):
    
    """
    plots the hypnogram in rows of n-hours as specified by the
    kwarg n.
    """
    
    n_columns = np.int(n * 3600)
    n_rows = np.int(np.ceil(smooth_score.shape[0] / n_columns))
    extend_to = np.int(n_rows * n_columns)
    pad_value = np.unique(smooth_score).max() + 1
    smooth_score_padded = np.pad(smooth_score,
                                (0, extend_to - smooth_score.shape[0]),
                                'constant',
                                constant_values=(0, pad_value))
    
    smooth_score_padded_sq = smooth_score_padded.reshape(
        n_rows, n_columns)[::-1] # reverse order so it reads right-to-left, top-to-bottom
    
    n_scores = np.unique(smooth_score).size
    default_color_cycle = plt.rcParams['axes.prop_cycle'].by_key()['color']
    color_cycle = ['darkgrey'] + default_color_cycle
    # TODO: fix this! (below).
    if extend_to == smooth_score.shape[0]:
        # do nothing.
        pass
    else:
        # add white to the color cycle for the padded values.
        color_cycle = color_cycle[:n_scores] + ['w']
    cmap = plt.cm.colors.ListedColormap(color_cycle)
    ax.pcolormesh(smooth_score_padded_sq, cmap=cmap)
        
def _plot_spectrogram_against_score(fig, Sxx, smooth_score, bandwidth, window, p1p2, cmap):
    
    """
    plots the spectrogram for a given bandwidth and window of time.
    """
    
    try:
        assert Sxx.shape[0] == 501 # just making sure the spectrogram was computed with known parameters.
    except AssertionError:
        print('Sxx with shape of {} x {}'.format(Sxx.shape[0], Sxx.shape[1]))
    
    start, stop = window[0], window[1]
    high_pass, low_pass = int(bandwidth[0]*2), int(bandwidth[1]*2) # need to be int to be used as indices.
    p1, p2 = p1p2
    sxx = Sxx[high_pass:low_pass+1][:, start:stop+1]
    vmin, vmax = np.percentile(sxx, p1), np.percentile(sxx, p2)
    
    try:
        assert vmin != 0
    except AssertionError:
        print('vmin cannot be 0: vmin = {}'.format(vmin))
    
    score_cmap, norm = from_levels_and_colors([-1,0,1,2,3], ['darkgrey', 'C0', 'C1', 'C2'])
    score_sample = np.atleast_2d(smooth_score[window[0]:window[1]+1])
    
    gs = gridspec.GridSpec(2,1, height_ratios=[9,1])
    ax0, ax1 = fig.add_subplot(gs[0]), fig.add_subplot(gs[1])
    ax0.pcolormesh(sxx, norm=LogNorm(vmin, vmax), cmap=cmap)
    ax1.pcolormesh(score_sample, cmap=score_cmap)
    ax0.get_shared_x_axes().join(ax0, ax1) # shares the x-axis.
    
def _plot_scatterplot(ax, xy, Sxx, z, vel, mode, s, cmap, p1p2):
    
    p1, p2 = p1p2
    
    if mode == 'density':
        vmin, vmax = np.percentile(z, p1), np.percentile(z, p2)
        idx = np.argsort(z) # plots the hottest colors on top.
        ax.scatter(xy[idx][:,0], xy[idx][:,1], s, c=z[idx], norm=LogNorm(vmin, vmax), cmap=cmap)
        
    elif mode == 'velocity':
        sort_idx = np.argsort(vel)
        vel_min = vel[sort_idx[1]]
        vel_range  = vel.max() - vel_min
        ofs1, ofs2 = p1/100 * vel_range, p2/100 * vel_range
        vmin, vmax = vel_min+ofs1, vel_min+ofs2
        ax.scatter(xy[sort_idx][:,0], xy[sort_idx][:,1], s, c=vel[sort_idx], norm=LogNorm(vmin, vmax), cmap=cmap)
        
    elif mode == 'delta':
        sxx = Sxx[1:9]
        delta = sxx.sum(0)
        idx = np.argsort(delta)
        delta_range = delta.max() - delta.min()
        ofs1, ofs2 = p1/100*delta_range, p2/100*delta_range
        vmin, vmax = delta.min()+ofs1, delta.min()+ofs2
        ax.scatter(xy[idx][:,0], xy[idx][:,1], s, c=delta[idx], cmap=cmap, norm=LogNorm(vmin, vmax))
        
    elif mode == 'theta':
        sxx = Sxx[9:15]
        theta = sxx.sum(0)
        idx = np.argsort(theta)
        vmin, vmax = np.percentile(theta, p1), np.percentile(theta, p2)
        ax.scatter(xy[idx][:,0], xy[idx][:,1], s, c=theta[idx], cmap=cmap, norm=LogNorm(vmin, vmax))
        
    elif mode == 'alpha':
        sxx = Sxx[9:27]
        alpha= sxx.sum(0)
        idx = np.argsort(alpha)
        vmin, vmax = np.percentile(alpha, p1), np.percentile(alpha, p2)
        ax.scatter(xy[idx][:,0], xy[idx][:,1], s, c=alpha[idx], cmap=cmap, norm=LogNorm(vmin, vmax))
        
    elif mode == 'beta':
        sxx = Sxx[27:81]
        beta= sxx.sum(0)
        idx = np.argsort(beta)
        vmin, vmax = np.percentile(beta, p1), np.percentile(beta, p2)
        ax.scatter(xy[idx][:,0], xy[idx][:,1], s, c=beta[idx], cmap=cmap, norm=LogNorm(vmin, vmax))
     
    elif mode == 'gamma':   
        sxx = Sxx[81:]
        gamma = sxx.sum(0)
        idx = np.argsort(gamma)
        vmin, vmax = np.percentile(gamma, p1), np.percentile(gamma, p2)
        ax.scatter(xy[idx][:,0], xy[idx][:,1], s, c=gamma[idx], cmap=cmap, norm=LogNorm(vmin, vmax))
        
    elif mode == 'WT':
        sxx = Sxx[14:24]
        band = sxx.sum(0)
        idx = np.argsort(band)
        vmin, vmax = np.percentile(band, p1), np.percentile(band, p2)
        ax.scatter(xy[idx][:,0], xy[idx][:,1], s, c=band[idx], cmap=cmap, norm=LogNorm(vmin, vmax))