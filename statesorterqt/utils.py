from __future__ import division
import datetime
from matplotlib import pylab as plt
import matplotlib as mpl
from math import sqrt
import numpy as np
from scipy.signal import get_window, spectrogram
from scipy.stats import gaussian_kde
from sklearn.decomposition import PCA

def _smooth_hanning(x, window_len=10, repeat=1):
     
    #s = np.r_[x[window_len-1:0:-1], x, x[-2:-window_len-1:-1]]
    w = np.hanning(window_len)
    w = w/w.sum()
    for i in range(repeat):
        x=np.convolve(w, x, mode='same')
    return x

def _smooth_boxcar(y, window_len=10):
    
    box = np.ones(window_len)/window_len
    y_smooth = np.convolve(y, box, mode='same')
    return y_smooth

def _euclidean_distance(a, b):
    
    """
    calculates the eclidean distance between two points.
    """
    
    dist = sqrt((a[0]-b[0])**2 + (a[1]-b[1])**2)
    return dist

def _get_velocity(xy):
    """
    """
    vel = [0]
    for i in range(1,len(xy),1):
        dist = abs(_euclidean_distance(xy[i-1], xy[i]))
        vel.append(dist/1)
    return np.array(vel)
        
SYSTEM_TIME = np.dtype([
    ('Year', np.short),
    ('Month', np.short),
    ('DayofWeek', np.short),
    ('WDay', np.short),
    ('WHour', np.short),
    ('WMinute', np.short),
    ('WSecond', np.short),
    ('WMilliSecond', np.short)])

FILTER_INFO = np.dtype([
    ('dwCorner_mHz', np.uint32),
    ('dwOrder', np.uint32),
    ('wType', np.uint16)])

NSX_G_HEADER = np.dtype([
    ('achFileType', (str, 8)),
    ('ByMajor', np.uint8),
    ('ByMinor', np.uint8),
    ('nBytesinHeader', np.uint32),
    ('Label', (str, 16)),
    ('Comment', (str, 256)),
    ('NPeriod', np.uint32),
    ('NTimeIndexResolution', np.uint32),
    ('SysTime', SYSTEM_TIME),
    ('NChannelCount', np.uint32)])

NSX_CC_HEADER = np.dtype([
    ('achType', 'S2'),
    ('dwElectrodeID', np.uint16),
    ('szLabel', 'S16'),
    ('byPhysicalConnector', np.uint8),
    ('byPhysicalPin', np.uint8),
    ('iMinDigital', np.int16),
    ('iMaxDigital', np.int16),
    ('iMinAnalog', np.int16),
    ('iMaxAnalog', np.int16),
    ('szUnits', 'S16'),
    ('isFltHigh', FILTER_INFO),
    ('isFltLow', FILTER_INFO)])

NSX_EX_HEADER = np.dtype([
    ('byType', np.uint8),
    ('dwTimeIndex', np.uint32),
    ('cnNumOfDataPoints', np.uint32)])

def _read_ns1(file_name):
    
    """
    does the same as _read_data_nsx except that it does not
    "chunk" the file as it is read.
    """
    
    with open(file_name, 'rb') as o_file:
        nsx_header = np.fromfile(o_file, dtype=NSX_G_HEADER, count=1)
        n_channels = nsx_header['NChannelCount'][0]
        nsx_cc_headers = np.fromfile(
            o_file, dtype=NSX_CC_HEADER, count=n_channels)
        tmp_header = np.fromfile(o_file, dtype=NSX_EX_HEADER, count=1)
        nsx_ex_headers = np.hstack((tmp_header))
        data_pos = int(nsx_header[0]['nBytesinHeader'])
        data_pos += NSX_EX_HEADER.itemsize
        data_all = np.memmap(o_file, np.int16, 'r', data_pos, 
                               (nsx_ex_headers['cnNumOfDataPoints'][0], 
                                n_channels))
        return data_all

def _process_ns1(fid, return_spectra=False):
    
    """
    reads the .ns1 files and performs all of the preprocessing,
    and signal processing on the recordings. returns the xy-
    coordinates (with respect for the 2D state-space) for each
    1 second of the recording.
    """
    
    data_raw = _read_ns1(fid)
    time_in_secs = data_raw.shape[0] / 500
        
    if time_in_secs >= 3600 * 25:
        # pops the last hour and slices the last 24h prior to that.
        data_raw = data_raw[-3600*25*500:-3600*500, :]

    fs = 500
    n_seconds = 2
    ol = fs * n_seconds / 2
    
    r1, r2, Sxx_all = [], [], []
    for ch in data_raw.T: #transpose to iterate through channels
        w = get_window('hann', fs * n_seconds)
        f, t, Sxx = spectrogram(ch, fs=fs, window=w, noverlap=ol, mode='magnitude')
        Sxx_all.append(Sxx)
        _r1 = np.array([sum(each_tbin[1:40+1]) / sum(each_tbin[1:110+1]) for each_tbin in Sxx.T])
        _r2 = np.array([sum(each_tbin[1:9+1]) / sum(each_tbin[1:18+1]) for each_tbin in Sxx.T])
        _delta = np.array([sum(each_tbin[2:9+1]) for each_tbin in Sxx.T])
        r1.append(_r1)
        r2.append(_r2)
        
    r1, r2 = np.array(r1), np.array(r2)
    
    pca = PCA(n_components=1)
    r1_pc1 = pca.fit_transform(r1.T).flatten()
    r2_pc1 = pca.fit_transform(r2.T).flatten()
    
    r1_smooth, r2_smooth = _smooth_hanning(r1_pc1, 10, 2), \
        _smooth_hanning(r2_pc1, 10, 2)
    X = np.array(zip(r2_smooth, r1_smooth))
    
    if return_spectra:
        return X, Sxx_all
    
    else:
        return X
    
def _get_ctime(fid):
    
    """
    returns a dataetime.datetime type object for the time of creation of
    the given .ns1 file.
    """

    with open(fid, 'rb') as o_file:
        nsx_header = np.fromfile(o_file, dtype=NSX_G_HEADER, count=1)
        systime = nsx_header['SysTime']
        year, month, day = [systime[key][0] for key in ['Year', 'Month', 'WDay']]
        hour, min, sec = [systime[key][0] for key in ['WHour', 'WMinute', 'WSecond']]
        ctime = datetime.datetime(year, month, day, hour, min, sec)
        return ctime

def _run_gkde(xy):
    
    """
    performs kernel density extimation on xy-data with a Gaussian kernel.
    
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.gaussian_kde.html
    """
    
    x, y = xy[:,0], xy[:,1]
    xy = np.vstack([x,y])
    estimator = gaussian_kde(xy)
    z = estimator(xy)
    
    return z
    
    # (below) moved to the state sorter widget.
    
    #if return_colors == True:
    #    c_norm = mpl.colors.Normalize(vmin=z.min(), vmax=z.max())
    #    jet = plt.cm.get_cmap('jet')
    #    scalarMap = plt.cm.ScalarMappable(norm=c_norm, cmap=jet)
    #    colors = scalarMap.to_rgba(z) * 255
    #    return z, colors
    
def _connect_action(action, function):
    
    """
    connects the given QAction object with the given function.
    
    :param QAction action: a instance of QAction.
    :param function function: a function to connect to the action.
    """
    
    action.triggered.connect(function)
    
def _clear_layout(layout):
    
    """
    clears all of the widgets and children layouts from the
    parent layout.
    
    :param QBoxLayout layout: instance of QHBoxLayout or QVBoxLayout
    """
    
    index = layout.count() - 1
    while index >= 0:
        child = layout.takeAt(index)
        if child.widget():
            child.widget().setParent(None)
        elif child.layout():
            
            # old way, only goes 1-layer deep.
            #subindex = child.count()
            #while subindex >= 0:
            #    grandchild = child.takeAt(subindex)
            #    if grandchild.widget():
            #        grandchild.widget().setParent(None)
            #    subindex -= 1
            
            # new way, infinite layers deep.
            _clear_layout(child.layout())
            
        index -= 1
    
def _remove_child(parent, child):
    
    """
    removes the child widget or layout from the parent layout.
    
    :param QBoxLayout layout: parent layout
    :param QWidget widget: target widget
    """
        
    index = parent.count() - 1
    while index >= 0:
        item = parent.itemAt(index)
        if item.widget() is child:
            child.setParent(None)
        elif item.layout() is child:
            _clear_layout(child)
        index -= 1
        
def _remove_axis_frame(ax, remove_ticks=False):
    
    """
    removes the spines from the ax.
    
    :param bool remove_ticks: if True will remove the x- and y-ticks as well.
    """
    
    spines = ['top', 'bottom', 'right', 'left']
    for spine in spines:
        ax.spines[spine].set_visible(False)
        
    if remove_ticks is True:
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])
    
#def _populate_summary_dataframe(cluster_masks_smooth,
#                                score_smooth,
#                                state_ids_dict,
#                                bout_crit_dict,
#                                binsize=1):
#    
#    """
#    """
#    columns = ['wake_%time', 'nrem_%time', 'rem_%time',
#                   'wake_avg-dur', 'nrem_avg-dur', 'rem_avg-dur',
#                   'wake_n-bouts', 'nrem_n-bouts', 'rem_n-bouts',
#                   'wake_avg-dur_sterr', 'nrem_avg-dur_sterr', 'rem_avg-dur_sterr',
#                   'wake2nrem', 'wake2rem', 'nrem2wake',
#                   'rem2wake', 'nrem2rem', 'rem2nrem']
#    df = pd.DataFrame(columns=columns)
#    
#    ids = state_ids_dict
#    id_wake, id_nrem, id_rem = ids['wake'], ids['nrem'], ids['rem']
#    
#    masks = np.array(cluster_masks_smooth)
#    n_seconds = score_smooth.shape[0]
#    step = binsize * 3600
#    for i, n in enumerate(range(0, n_seconds, step)):
#        row_idx = i
#        
#        slice_masks = masks[:,n:n+step]
#        for id, mask in enumerate(slice_masks):
#            val = sum(mask) / float(mask.shape[0])
#            if id == id_wake:
#                df.loc[row_idx, 'wake_%time'] = val
#            elif id == id_nrem:
#                df.loc[row_idx, 'nrem_%time'] = val
#            elif id == id_rem:
#                df.loc[row_idx, 'rem_%time'] = val
#                
#        slice_score = score_smooth[n:n+step]
#        _dict, all_bouts = _get_bout_duration_average(slice_score,
#                                                      state_ids_dict,
#                                                      bout_crit_dict)
#        for key in _dict.keys():
#            avg_dur, n_bouts, sterr = _dict[key]
#            if key == 'wake':
#                df.loc[row_idx, 'wake_avg-dur'] = avg_dur
#                df.loc[row_idx, 'wake_n-bouts'] = n_bouts
#                df.loc[row_idx, 'wake_avg-dur_sterr'] = sterr
#            elif key == 'nrem':
#                df.loc[row_idx, 'nrem_avg-dur'] = avg_dur
#                df.loc[row_idx, 'nrem_n-bouts'] = n_bouts
#                df.loc[row_idx, 'nrem_avg-dur_sterr'] = sterr
#            elif key == 'rem':
#                df.loc[row_idx, 'rem_avg-dur'] = avg_dur
#                df.loc[row_idx, 'rem_n-bouts'] = n_bouts
#                df.loc[row_idx, 'rem_avg-dur_sterr'] = sterr
#        
#        vst_count_dict = _count_valid_state_transitions(slice_score,
#                                                       state_ids_dict)
#        for key in vst_count_dict.keys():
#            if key == 'wake2nrem':
#                df.loc[row_idx, key] = vst_count_dict[key]
#            elif key == 'wake2rem':
#                df.loc[row_idx, key] = vst_count_dict[key]
#            elif key == 'nrem2wake':
#                df.loc[row_idx, key] = vst_count_dict[key]
#            elif key == 'nrem2rem':
#                df.loc[row_idx, key] = vst_count_dict[key]
#            elif key == 'rem2wake':
#                df.loc[row_idx, key] = vst_count_dict[key]
#            elif key == 'rem2nrem':
#                df.loc[row_idx, key] = vst_count_dict[key]    
#        
#    return df   