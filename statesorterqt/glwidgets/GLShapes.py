'''
Created on Feb 28, 2015

@author: alessandro
'''
__updated__ = '2017-04-11'

import itertools as it
import numpy as np

import OpenGL.GL as gl
import OpenGL.GLUT as glut


FAR_PLANE = 1000
MAX_GRID_3D = 10
MAX_GRID_2D = 10


def paint_point(coord,
                pointSize=4, color=(0, 1, 0), alpha=.5):

    # sets the linewidth
    gl.glPointSize(pointSize)
    # color
    gl.glColor(*color)
    alpha = (alpha,)
    color = color + alpha
    # begin
    gl.glBegin(gl.GL_POINTS)

    gl.glVertex3d(*tuple(coord.flatten()))

    # end the plotting
    gl.glEnd()


def paint_line(orig, dest,
               lineWidth=2, color=(1, 0, 0), alpha=.7):

    # sets the linewidth
    gl.glLineWidth(lineWidth)
    # color and alpha
    alpha = (alpha,)
    color = color + alpha
    gl.glColor(*color)
    # begin
    gl.glDisable(gl.GL_DEPTH_TEST)
    gl.glBegin(gl.GL_LINES)

    gl.glVertex3d(*tuple(orig))
    gl.glVertex3d(*tuple(dest))

    # end the plotting
    gl.glEnd()


def paint_loop_line(pointlist=None,
                    lineWidth=2, color=(1, 0, 0), alpha=.7):

    # sets the linewidth
    gl.glLineWidth(lineWidth)
    # color and alpha
    alpha = (alpha,)
    color = color + alpha
    gl.glColor(*color)
    # begin
    # gl.glBegin(gl.GL_LINE_LOOP)
    gl.glDisable(gl.GL_DEPTH_TEST)
    gl.glBegin(gl.GL_LINE_STRIP)

    for point in pointlist:
        gl.glVertex3d(*tuple(point))

    # end the plotting
    gl.glEnd()


def paint_axis2D(xlim=None, ylim=None,
                 lineWidth=1.5, color=(.8, .8, .8)):
    '''
    This paints the 2D axis. Make sure it is called inside the paintGL
    function
    '''

    if xlim is None:
        xlim = FAR_PLANE
    if ylim is None:
        ylim = FAR_PLANE

    # sets the linewidth
    gl.glLineWidth(lineWidth)
    # color and alpha
    alpha = (.2,)
    color = color + alpha
    gl.glColor(*color)
    # begin
    gl.glBegin(gl.GL_LINES)
    gl.glVertex3d(0, - ylim, 0)
    gl.glVertex3d(0, ylim, 0)
    gl.glVertex3d(-xlim, 0, 0)
    gl.glVertex3d(xlim, 0, 0)
    # end the plotting
    gl.glEnd()
    # flush to screen
    gl.glFlush()


def paint_axis3D(xlim=5, ylim=5, zlim=5,
                 lineWidth=1.5, color=(1, 1, 1)):
    '''
    This paints the 3D axis. Make sure it is called inside the paintGL
    function
    '''

    if xlim is None:
        xlim = FAR_PLANE
    if ylim is None:
        ylim = FAR_PLANE
    if zlim is None:
        zlim = FAR_PLANE

    # sets the linewidth
    gl.glLineWidth(lineWidth)
    # color
    # gl.glColor(*color)
    # begin
    gl.glBegin(gl.GL_LINES)
    gl.glColor((0, 0, 1))
    gl.glVertex3d(0, 0, 0)
    gl.glVertex3d(xlim, 0, 0)
    gl.glColor((1, 0, 0))
    gl.glVertex3d(0, 0, 0)
    gl.glVertex3d(0, ylim, 0)
    gl.glColor((0, 1, 0))
    gl.glVertex3d(0, 0, 0)
    gl.glVertex3d(0, 0, zlim)
    # end the plotting
    gl.glEnd()
    # flush to screen
    gl.glFlush()


def paint_cube(xlim=None, ylim=None, zlim=None, step=1,
               pointSize=1, color=(0, 1, 1)):
    '''
    makes a cube
    '''

    if xlim is None:
        xlim = 1
    if ylim is None:
        ylim = 1
    if zlim is None:
        zlim = 1

    gl.glPointSize(pointSize)
    gl.glColor(*color)

    gl.glBegin(gl.GL_LINE_STRIP)
    gl.glVertex3d(0, 0, 0)
    gl.glVertex3d(1, 0, 0)
    gl.glVertex3d(1, 1, 0)
    gl.glVertex3d(0, 1, 0)
    gl.glVertex3d(0, 0, 0)
    gl.glVertex3d(0, 0, 1)
    gl.glVertex3d(1, 0, 1)
    gl.glVertex3d(1, 0, 0)
    gl.glVertex3d(1, 0, 1)
    gl.glVertex3d(1, 1, 1)
    gl.glVertex3d(1, 1, 0)
    gl.glVertex3d(1, 1, 1)
    gl.glVertex3d(0, 1, 1)
    gl.glVertex3d(0, 1, 0)
    gl.glVertex3d(0, 1, 1)
    gl.glVertex3d(0, 0, 1)

    gl.glEnd()
    gl.glFlush()


def paint_grid3D(xlim=None, ylim=None, zlim=None, xstep=1,
                 ystep=1, zstep=1, pointSize=1, color=(.7, .7, .7)):

    if xlim is None:
        xlim = (-MAX_GRID_3D, MAX_GRID_3D)
    if ylim is None:
        ylim = (-MAX_GRID_3D, MAX_GRID_3D)
    if zlim is None:
        zlim = (-MAX_GRID_3D, MAX_GRID_3D)

    gl.glPointSize(pointSize)
    gl.glColor(*color)

    gl.glBegin(gl.GL_POINTS)
    for triplet in it.product(range(xlim[0], xlim[1], xstep),
                              range(ylim[0], ylim[1], ystep),
                              range(zlim[0], zlim[1], zstep)
                              ):
        gl.glVertex3d(*triplet)

    gl.glEnd()
    gl.glFlush()


def paint_grid2D(xlim=None, ylim=None, xstep=1, ystep=1,
                 pointSize=1, color=(.7, .7, .7)):

    if xlim is None:
        xlim = (-MAX_GRID_2D, MAX_GRID_2D)
    if ylim is None:
        ylim = (-MAX_GRID_2D, MAX_GRID_2D)

    gl.glPointSize(pointSize)
    gl.glColor(*color)

    gl.glBegin(gl.GL_POINTS)
    for triplet in it.product(np.arange(ylim[0], ylim[1], xstep),
                              np.arange(ylim[0], ylim[1], ystep),
                              (0,)
                              ):
        gl.glVertex3d(*triplet)

    gl.glEnd()
    gl.glFlush()


def glut_print2D(x, y, font=glut.GLUT_BITMAP_HELVETICA_18, text="test", rgba=(1, 1, 1, 1), absolute=False):

    blending = False
    if gl.glIsEnabled(gl.GL_BLEND):
        blending = True

    # glEnable(GL_BLEND)
    gl.glColor(*rgba)
    if absolute:
        gl.glWindowPos2f(x, y)
    else:
        gl.glRasterPos2f(x, y)

    for ch in text:
        glut.glutBitmapCharacter(font, gl.ctypes.c_int(ord(ch)))

    if not blending:
        gl.glDisable(gl.GL_BLEND)


def glut_print3D(x, y, z, font=glut.GLUT_BITMAP_HELVETICA_18, text="test", rgba=(1, 1, 1, 1), absolute=False):

    blending = False
    if gl.glIsEnabled(gl.GL_BLEND):
        blending = True

    # glEnable(GL_BLEND)
    gl.glColor(*rgba)
    if absolute:
        gl.glWindowPos3f(x, y, z)
    else:
        gl.glRasterPos3f(x, y, z)

    for ch in text:
        glut.glutBitmapCharacter(font, gl.ctypes.c_int(ord(ch)))

    if not blending:
        gl.glDisable(gl.GL_BLEND)


if __name__ == '__main__':
    pass
