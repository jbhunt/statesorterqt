'''
Created on Feb 28, 2015

@author: alessandro
'''
import numpy as np

import OpenGL.GLU as glu
import OpenGL.GL as gl


__updated__ = '2016-03-23'


def project(x=0., y=0., z=0.):
    '''
    Compute the 3D opengl coordinates for 3 coordinates.
    @param x,y: coordinates from canvas taken with mouse position
    @param z: coordinate for z-axis
    @return; (gl_x, gl_y, gl_z) tuple corresponding to coordinates in OpenGL
             context
    '''

    modelview = np.matrix(gl.glGetDoublev(gl.GL_MODELVIEW_MATRIX))
    projection = np.matrix(gl.glGetDoublev(gl.GL_PROJECTION_MATRIX))
    viewport = gl.glGetIntegerv(gl.GL_VIEWPORT)

    modelX = float(x)
    modelY = float(y)
    modelZ = float(z)
    return glu.gluProject(modelX, modelY, modelZ,
                          modelview, projection, viewport)


def project_vertices(vertices=None):
    '''
    Given the vertices coordinates in the opengl world it maps the
    coordinate of the vertices given to the current viewport.

    :param vertices: array of vertices
    :type vertices: <array>
    '''

    vertices = vertices * 1.
    modelview = np.matrix(gl.glGetDoublev(gl.GL_MODELVIEW_MATRIX))
    projection = np.matrix(gl.glGetDoublev(gl.GL_PROJECTION_MATRIX))

    Matrix = projection * modelview
    Matrix = np.array(Matrix).reshape(4, 4).T
    print(Matrix)
    vertices = np.concatenate(
        (vertices, np.ones((vertices.shape[0], 1))), axis=1)
    Trans = Matrix.dot(vertices.T)
    X = Trans[0, :] / Trans[3, :]
    Y = Trans[1, :] / Trans[3, :]
    return X, Y


def project_vertices_qt(GLWidget, vertices=None):
    '''
    Given the vertices coordinates in the opengl world it maps the coordinate of
    the vertices given to the current viewport. This differs from
    project_vertices in that it uses the projection matrices from the qt widget.

    :param vertices: array of vertices
    :type vertices: <array>
    '''

    vertices = vertices.astype(np.float)

    Matrix = GLWidget.projectionMatrix() * GLWidget.viewMatrix()
    Matrix = np.array(Matrix.data()).reshape(4, 4).T
    if vertices.shape[1] == 2:
        vertices = np.concatenate(
            (vertices, np.ones((vertices.shape[0], 1))), axis=1)
    vertices = np.concatenate(
        (vertices, np.ones((vertices.shape[0], 1))), axis=1)
    Trans = Matrix.dot(vertices.T)
    Trans[0, :] = Trans[0, :] / Trans[3, :]
    Trans[1, :] = Trans[1, :] / Trans[3, :]
    return Trans[0:2, :].T


def unproject(x, y, z=0, modelview_mat=None, proj_mat=None, viewport=None):
    '''
    Compute the 3D opengl coordinates from the viewport.
    @param x,y: coordinates from canvas taken with mouse position
    @param z: coordinate for z-axis
    @return; (gl_x, gl_y, gl_z) tuple corresponding to coordinates in OpenGL
             context
    '''
    if modelview_mat is None:
        modelview_mat = np.matrix(gl.glGetDoublev(gl.GL_MODELVIEW_MATRIX))
    if proj_mat is None:
        proj_mat = np.matrix(gl.glGetDoublev(gl.GL_PROJECTION_MATRIX))
    if viewport is None:
        viewport = gl.glGetIntegerv(gl.GL_VIEWPORT)

    winX = float(x)
    winY = float(viewport[3] - float(y))
    winZ = float(z)
    return glu.gluUnProject(winX, winY, winZ, modelview_mat, proj_mat, viewport)
