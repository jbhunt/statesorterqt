'''
Created on Feb 27, 2015

@author: alessandro
'''

import sys
import logging

from pprint import pformat as pf

import numpy as np
import scipy.spatial as spsp

from PyQt4 import QtGui, QtCore
from PyQt4 import QtOpenGL as QtGL

import OpenGL.GL as gl
import OpenGL.arrays.vbo as glvbo
from OpenGL.arrays.numpymodule import GL_TYPE_TO_ARRAY_MAPPING as gltnp

import statesorterqt.glwidgets.GLShapes as glsh
import statesorterqt.glwidgets.GLUtility as glutils

from matplotlib.path import Path as Path
import pdb  # @UnusedImport

# This set defines the current implemented interactive tools see
# CanvasInteractive.ii_tool for more information
INTERACTIVE_TOOLS = {'hands_free', 'point', 'line', ''}

__updated__ = '2017-04-09'

# Logger -----------------------------------------------------------------
logger = logging.getLogger(__name__)


PAINT_DICT = {'line': gl.GL_LINE_STRIP,
              'dot': gl.GL_POINTS,
              }


def repr_no_parent(self):
    orig = QtCore.QObject.__repr__(self)
    pr = self.__dict__.copy()
    # remove the parent entry
    pr.pop('parent', None)
    # removes all '_' from fields for the properties
    pr = pr.items()
    pr = [(x, y) if x[0] != '_' else (x[1:], y)
          for x, y in pr]
    # removes all entries still beginning with '_' 'secret methods'
    pr = [(x, y) for x, y in pr if x[0] != '_']
    return orig + '\n' + 'Properties:\n' + pf(dict(pr))


def float_array(Input):

    return np.array(Input).astype('f')


class FunctionWrapper(object):

    # https://wiki.python.org/moin/PythonDecoratorLibrary

    def __init__(self, precondition, postcondition, function):
        # initialize the lists
        self._pre = []
        self._post = []

        # adds the first elements
        self._pre.append(precondition)
        self._post.append(postcondition)
        self._func = function

    def add_pre(self, precondition):
        self._pre.append(precondition)

    def add_post(self, postcondition):
        self._post.append(postcondition)

    def __call__(self, *args, **kwargs):

        for precondition in self._pre:
            if precondition:
                msg = "pre"
                logger.debug(msg)
                precondition(*args, **kwargs)

        msg = "original"
        logger.debug(msg)
        result = self._func(*args, **kwargs)

        for postcondition in self._post:
            if postcondition:
                msg = "post"
                logger.debug(msg)
                # postcondition(result, *args, **kwargs)
                postcondition(*args, **kwargs)
        return result


class DecoratedRenderer(object):

    __repr__ = repr_no_parent

    def __init__(self, parent=None):

        if parent is not None:

            if isinstance(parent.paintGL, FunctionWrapper):
                msg = "adding to existing decorator"
                parent.paintGL.add_pre(self.pre_paintGL)
                parent.paintGL.add_post(self.post_paintGL)

            else:
                msg = "Decorating"
                logger.debug(msg)
                parent.renderer = FunctionWrapper(
                    self.pre_paintGL, self.post_paintGL, parent.renderer)

            self.parent = parent

    def pre_paintGL(self):
        pass

    def post_paintGL(self):
        pass


class BasicCanvas(QtGL.QGLWidget):

    '''
    Handles OpenGL plotting.
    This class is not used anymore and is obsolete.
    '''

    __repr__ = repr_no_parent
    _bc_default_color = np.array([170, 170, 170, 255], dtype='f')
    _gtype = 'dot'

    def __init__(self, parent=None):

        super(BasicCanvas, self).__init__(parent)

        self.bc_background = (.35, .35, .35, .8)
        self.bc_pointsize = 1.5
        self.bc_linewidth = 1.5
        self.vertex_array = np.array([], dtype='f')
        self.bc_plot_indexes = np.arange(self.vertex_array.shape[0])

        self.resize(300, 300)

    def initializeGL(self):
        '''
        This virtual function is called once before the first call to paintGL()
        or resizeGL(), and then once whenever the widget has been assigned a
        new QGLContext. [PyQt4 Official Documentation]
        '''

        # background color
        # Specify the red, green, blue, and alpha values used when the color
        gl.glClearColor(*self.bc_background)

    def pre_paintGL(self):

        # Staff that is done everytime before the real paint
        # gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
        gl.glEnable(gl.GL_DEPTH_TEST)
        gl.glEnable(gl.GL_BLEND)
        # blending type
        # on alpha
        # gl.glBlendEquation(gl.GL_FUNC_ADD)
        gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)
        # on color
        # gl.glBlendFunc(gl.GL_SRC_COLOR, gl.GL_ONE_MINUS_SRC_COLOR)
        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
        gl.glMatrixMode(gl.GL_PROJECTION)
        gl.glLoadIdentity()
        gl.glOrtho(0, 1, -1, 1, -10, 1000)

    def paintGL(self):
        '''
        This virtual function is called whenever the widget needs to be
        painted. Reimplement it in a subclass. [PyQt4 Official Documentation]
        '''

        if hasattr(self, 'pre_paintGL'):
            self.pre_paintGL()

        if hasattr(self, 'renderer'):
            self.renderer()

        if hasattr(self, 'post_paintGL'):
            self.post_paintGL()

    def post_paintGL(self):
        pass

    def renderer(self):

        # draw a cube with new opengl directives
        msg = "rendering"
        logger.debug(msg)
        gl.glLineWidth(self.bc_linewidth)
        gl.glPointSize(self.bc_pointsize)

        #: binds the vertex buffer object and points to it

        gl.glEnableClientState(gl.GL_VERTEX_ARRAY)
        gl.glVertexPointer(
            self.bc_vertex_array_dims, gl.GL_FLOAT, 0, self.bc_vertex_array[self.bc_plot_indexes, :])

        #: binds the color buffer object and points to it
        gl.glEnableClientState(gl.GL_COLOR_ARRAY)
        gl.glColorPointer(
            4, gl.GL_UNSIGNED_BYTE, 0, self.bc_color_array[self.bc_plot_indexes, :])

        gl.glDrawArrays(
            PAINT_DICT[self._gtype], 0, self.bc_plot_indexes.sum())

        gl.glDisableClientState(gl.GL_VERTEX_ARRAY)
        gl.glDisableClientState(gl.GL_COLOR_ARRAY)

    def projection(self):
        pass
        # if self.Camera.projection_ortho:

    # Properties -------------------------------------------------------------

    @property
    def vertex_array(self):
        return self.bc_vertex_array

    @vertex_array.setter
    def vertex_array(self, Input):
        self.bc_vertex_array = np.array(Input, dtype=np.float32)

        if self.bc_vertex_array.ndim == 1:
            x = np.arange(
                self.bc_vertex_array.shape[0], dtype=np.float32) / self.bc_vertex_array.shape[0]
            self.bc_vertex_array = np.vstack((x, self.bc_vertex_array)).T
            self.bc_vertex_array_dims = 2
        else:
            self.bc_vertex_array_dims = self.bc_vertex_array.shape[1]

        self.bc_color_array = np.tile(
            self._bc_default_color, (self.bc_vertex_array.shape[0], 1))

        self.bc_plot_indexes = np.ones(
            self.vertex_array.shape[0], dtype=np.bool)
        self.update()

    @property
    def gtype(self):
        return self._gtype

    @gtype.setter
    def gtype(self, Input):
        self._gtype = Input
        self.update()

    @property
    def modelview_mat(self):
        return np.array(
            self.viewMatrix().data()).reshape(4, 4)

    @property
    def proj_mat(self):
        return np.array(
            self.projectionMatrix().data()).reshape(4, 4)

    @property
    def viewport(self):
        return np.array(
            [0, 0, self.size().width(), self.size().height()]).astype(np.int32)


class BasicCanvasVBOs(QtGL.QGLWidget):

    '''
    Handles OpenGL plotting. It uses vbo to plot for fast performances. Vbos are
    stored internally as np recarrays.
    '''
    __use_vbo__ = True
    __repr__ = repr_no_parent

    _gtype = 'dot'
    bc_vertex_type = gl.GL_FLOAT
    bc_color_type = gl.GL_UNSIGNED_BYTE
    bc_indexes_type = gl.GL_UNSIGNED_INT
    _bc_default_color = np.array(
        [170, 170, 170, 255], dtype=gltnp[bc_color_type])

    def __init__(self, parent=None):

        super(BasicCanvasVBOs, self).__init__(parent)

        self.bc_background = (.4, .4, .4, .7)
        self.bc_pointsize = 1.5
        self.bc_linewidth = 1.5
        self.bc_points_per_vertex = 3

        self._bc_vertex_array = None
        self._bc_color_array = None
        self._bc_index_array = None

        # vertex vbo
        self.bc_vertex_array = np.zeros(0, dtype=gltnp[self.bc_vertex_type])
        # color vbo
        self.bc_color_array = np.zeros(0, dtype=gltnp[self.bc_color_type])
        # index vbo
        self.bc_index_array = np.zeros(0, dtype=gltnp[self.bc_indexes_type])

        # the following needs to be taken out
        #         self._buffer = np.zeros(
        #
        #             0, dtype=[('coordinates', np.float32, 3),
        #                       ('colors', np.uint8, 4),
        #                       ('entitynumber', np.int16, 1),
        #                       ('indexes', np.uint32, 1)])
        #
        #         self.__vbo__ = glvbo.VBO(self._buffer['coordinates'])
        #         self.__cbo__ = glvbo.VBO(self._buffer['colors'])
        #         self.__indexes__ = glvbo.VBO(self._buffer['indexes'],
        #                                      target=gl.GL_ELEMENT_ARRAY_BUFFER)
        self.resize(600, 600)

    def initializeGL(self):
        '''
        This virtual function is called once before the first call to paintGL()
        or resizeGL(), and then once whenever the widget has been assigned a
        new QGLContext. [PyQt4 Official Documentation]
        '''

        # background color
        # Specify the red, green, blue, and alpha values used when the color
        gl.glClearColor(*self.bc_background)

    def pre_paintGL(self):

        # Staff that is done everytime before the real paint
        # gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
        gl.glEnable(gl.GL_DEPTH_TEST)
        gl.glEnable(gl.GL_BLEND)
        # blending type
        # on alpha
        # gl.glBlendEquation(gl.GL_FUNC_ADD)
        gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)
        # on color
        # gl.glBlendFunc(gl.GL_SRC_COLOR, gl.GL_ONE_MINUS_SRC_COLOR)
        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
        gl.glMatrixMode(gl.GL_PROJECTION)
        gl.glLoadIdentity()
        gl.glOrtho(0, 1, -1, 1, -10, 1000)

    def paintGL(self):
        '''
        This virtual function is called whenever the widget needs to be
        painted. Reimplement it in a subclass. [PyQt4 Official Documentation]
        '''

        if hasattr(self, 'pre_paintGL'):
            self.pre_paintGL()

        if hasattr(self, 'renderer'):
            self.renderer()

        if hasattr(self, 'post_paintGL'):
            self.post_paintGL()

    def post_paintGL(self):
        pass

    def renderer(self):

        # draw a cube with new opengl directives
        msg = "BasicCanvasVBOs: rendering"
        logger.debug(msg)
        gl.glLineWidth(self.bc_linewidth)
        gl.glPointSize(self.bc_pointsize)

        #: binds the vertex buffer object and points to it
        self._bc_vertex_array.bind()
        gl.glEnableClientState(gl.GL_VERTEX_ARRAY)
        gl.glVertexPointer(self.bc_points_per_vertex,
                           gl.GL_FLOAT, 0, self._bc_vertex_array)

        #: binds the color buffer object and points to it
        gl.glEnableClientState(gl.GL_COLOR_ARRAY)

        self._bc_color_array.bind()
        gl.glColorPointer(4, gl.GL_UNSIGNED_BYTE, 0,
                          self._bc_color_array)

        self._bc_index_array.bind()

        gl.glDrawElements(PAINT_DICT[self._gtype],
                          self.bc_index_array.size,
                          gl.GL_UNSIGNED_INT, None)

        self._bc_vertex_array.unbind()
        self._bc_color_array.unbind()
        self._bc_index_array.unbind()

        gl.glDisableClientState(gl.GL_VERTEX_ARRAY)
        gl.glDisableClientState(gl.GL_COLOR_ARRAY)

    def projection(self):
        pass
        # if self.Camera.projection_ortho:

    def closeEvent(self, event):
        # deletes all vbos
        #         self.__cbo__.delete()
        #         self.__vbo__.delete()
        event.accept()
        pass

    def show(self):
        if not self.isVisible():
            # resetting bos
            # self.__cbo__.set_array(self._buffer['colors'])
            # self.__vbo__.set_array(self._buffer['coordinates'])
            # self.__indexes__.set_array(self._buffer['indexes'])
            pass
        super(BasicCanvasVBOs, self).show()

        # Properties ----------------------------------------------------------

    # TODO: this function should be named plot not vertex_array
    @property
    def vertex_array(self):
        return self.bc_vertex_array

    @vertex_array.setter
    def vertex_array(self, in_):

        if in_.ndim == 1:
            x = np.linspace(0, 1., in_.shape[0],
                            dtype=gltnp[self.bc_vertex_type])
            in_ = np.vstack((x, in_)).T

        self.bc_vertex_array = in_
        self.bc_color_array = np.tile(
            self._bc_default_color, (in_.shape[0], 1))
        self.bc_index_array = np.arange(
            in_.shape[0], dtype=gltnp[self.bc_indexes_type])

    @property
    def gtype(self):
        return self._gtype

    @gtype.setter
    def gtype(self, Input):
        self._gtype = Input
        self.update()

    @property
    def modelview_mat(self):
        return np.array(
            self.viewMatrix().data()).reshape(4, 4)

    @property
    def proj_mat(self):
        return np.array(
            self.projectionMatrix().data()).reshape(4, 4)

    @property
    def viewport(self):
        return np.array(
            [0, 0, self.size().width(), self.size().height()]).astype(np.int32)

    @property
    def bc_plot_indexes(self):
        bool_vec = np.full(self.bc_vertex_array.shape[0], False, np.bool8)
        bool_vec[self.bc_index_array] = True
        return bool_vec

    @bc_plot_indexes.setter
    def bc_plot_indexes(self, input_):
        self.bc_index_array = np.where(input_)[0].astype(np.uint32)

    @property
    def bc_vertex_array(self):
        if self._bc_vertex_array is not None:
            return self._bc_vertex_array.data

    @bc_vertex_array.setter
    def bc_vertex_array(self, in_):
        if not in_.dtype == gltnp[self.bc_vertex_type]:
            in_ = in_.astype(gltnp[self.bc_vertex_type])
            self.bc_points_per_vertex = in_.shape[1]
        if self._bc_vertex_array is None:
            self._bc_vertex_array = glvbo.VBO(in_)
        else:
            self._bc_vertex_array.set_array(in_)

    @property
    def bc_color_array(self):
        if self._bc_color_array is not None:
            return self._bc_color_array.data

    @bc_color_array.setter
    def bc_color_array(self, in_):
        if not in_.dtype == gltnp[self.bc_color_type]:
            in_ = in_.astype(gltnp[self.bc_color_type])
        if self._bc_color_array is None:
            self._bc_color_array = glvbo.VBO(in_)
        else:
            self._bc_color_array.set_array(in_)

    @property
    def bc_index_array(self):
        if self._bc_index_array is not None:
            return self._bc_index_array.data

    @bc_index_array.setter
    def bc_index_array(self, in_):
        if not in_.dtype == gltnp[self.bc_indexes_type]:
            in_ = in_.astype(gltnp[self.bc_indexes_type])
        if self._bc_index_array is None:
            self._bc_index_array = glvbo.VBO(
                in_, target=gl.GL_ELEMENT_ARRAY_BUFFER)
        else:
            self._bc_index_array.set_array(in_)


class CanvasGraphicItems(BasicCanvasVBOs):

    def __init__(self, parent=None):

        super(CanvasGraphicItems, self).__init__(parent)

        self._gi_axis_2D = False
        self._gi_axis_3D = False
        self._gi_grid_2D = False
        self._gi_grid_3D = False

        self._gi_grid_2D_lims = ((-10, 10), (-10, 10))
        self._gi_grid_2D_steps = ((1), (1))

        self._gi_grid_3D_lims = ((-10, 10), (-10, 10), (-10, 10))
        self._gi_grid_3D_steps = ((1), (1), (1))

        self._gi_xlabel = ''
        self._gi_ylabel = ''
        self._gi_zlabel = ''

    def renderer(self):
        super(CanvasGraphicItems, self).renderer()

        if self.gi_axis_2D | self.gi_axis_3D | self.gi_grid_2D | self.gi_grid_3D:
            msg = "Adding Graphic Items"
            logger.debug(msg)

            if self.gi_axis_3D:
                glsh.paint_axis3D()

            if self.gi_axis_2D:
                glsh.paint_axis2D()
                glsh.glut_print3D(
                    1, 0, 0, text=self._gi_xlabel, rgba=(0, 0, 1, .5), absolute=False)
                glsh.glut_print3D(
                    0, 1, 0, text=self._gi_ylabel, rgba=(1, 0, 0, .5), absolute=False)

            if self.gi_grid_2D:
                glsh.paint_grid2D(
                    *(self._gi_grid_2D_lims + self._gi_grid_2D_steps))

            if self.gi_grid_3D:
                glsh.paint_grid3D(
                    *(self._gi_grid_3D_lims + self._gi_grid_3D_steps))

    def post_paintGL(self):
        super(CanvasGraphicItems, self).post_paintGL()
        if self.gi_axis_3D:
            glsh.glut_print3D(
                5, 0, 0, text=self._gi_xlabel, rgba=(0, 0, 1, 1), absolute=False)
            glsh.glut_print3D(
                0, 5, 0, text=self._gi_ylabel, rgba=(1, 0, 0, 1), absolute=False)
            glsh.glut_print3D(
                0, 0, 5, text=self._gi_zlabel, rgba=(0, 1, 0, 1), absolute=False)

    @property
    def gi_axis_2D(self):
        return self._gi_axis_2D

    @gi_axis_2D.setter
    def gi_axis_2D(self, Input):
        self._gi_axis_2D = Input
        self.update()

    @property
    def gi_axis_3D(self):
        return self._gi_axis_3D

    @gi_axis_3D.setter
    def gi_axis_3D(self, Input):
        self._gi_axis_3D = Input
        self.update()

    @property
    def gi_grid_2D(self):
        return self._gi_grid_2D

    @gi_grid_2D.setter
    def gi_grid_2D(self, Input):
        self._gi_grid_2D = Input
        self.update()

    @property
    def gi_grid_3D(self):
        return self._gi_grid_3D

    @gi_grid_3D.setter
    def gi_grid_3D(self, Input):
        self._gi_grid_3D = Input
        self.update()

    @property
    def gi_xlabel(self):
        return self._gi_xlabel

    @gi_xlabel.setter
    def gi_xlabel(self, Input):
        self._gi_xlabel = Input

    @property
    def gi_ylabel(self):
        return self._gi_ylabel

    @gi_ylabel.setter
    def gi_ylabel(self, Input):
        self._gi_ylabel = Input

    @property
    def gi_zlabel(self):
        return self._gi_zlabel

    @gi_zlabel.setter
    def gi_zlabel(self, Input):
        self._gi_zlabel = Input


class CanvasInteractive(BasicCanvasVBOs):

    def __init__(self, parent=None):

        super(CanvasInteractive, self).__init__(parent)
        self.setFocusPolicy(QtCore.Qt.StrongFocus)

        #: keeps the current mouse position on the viewport
        self._ii_mouse_pos = np.array([0, 0])
        #: keeps the current mouse delta from the old position to the new
        self._ii_mouse_delta = np.array([0, 0])

        self.reset_inter_items()
        self.ii_tool = ''
        self._event_dictionary = dict()

    def reset_inter_items(self):

        self._ii_point = False
        self._ii_point_coord = np.array([0, 0, 0])

        self._ii_line = False
        self._ii_line_coords = np.array([[0., 0., 0.], [0., 0., 0.]])

        self._ii_hands_free_line = False
        self._ii_hands_free_line_coords = np.array([0, 0, 0])

        self._ii_key_pressed = 0
        self._ii_mouse_button = QtCore.Qt.NoButton

#         self._ii_project_entity = []
#         self._ii_project_data = []

    def post_paintGL(self):

        super(CanvasInteractive, self).post_paintGL()

        if self._ii_point:
            #msg = 'Point @{}'.format(self._ii_point_coord)
            # logger.info(msg)
            glsh.paint_point(self._ii_point_coord)

        if self._ii_line:
            #msg = 'Line @{}'.format(self._ii_line_coords)
            # logger.info(msg)
            orig = self._ii_line_coords[0, :]
            dest = self._ii_line_coords[1, :]
            glsh.paint_line(orig, dest)

        if self._ii_hands_free_line:
            # msg = 'hands free line @{}'.format(
            #    self._ii_hands_free_line_coords)
            # logger.debug(msg)
            glsh.paint_loop_line(self._ii_hands_free_line_coords)

    def keyPressEvent(self, QKeyEvent):
        # use it to define events on keyPresses
        # logger.info(QKeyEvent)
        self._ii_key_pressed = QKeyEvent.key()
        if self._ii_key_pressed == QtCore.Qt.Key_Shift \
                and self._ii_mouse_button == QtCore.Qt.NoButton:
            msg = "Shift pressed switching to point interaction"
            logger.info(msg)
            self._ii_prev_tool = self.ii_tool
            self.reset_inter_items()
            self.ii_tool = 'point'
            self._ii_point_coord = np.array(
                self.ii_coord).reshape(1, -1)
            self._ii_point = True
            self.reset_inter_items()
            self.update()

    def mouseMoveEvent(self, mouseEvent):

        self.ii_mouse_pos = (mouseEvent.x(), mouseEvent.y())
        self._ii_mouse_button = int(mouseEvent.buttons())
        msg = "ViewPort Coordinates: {}".format(self._ii_mouse_pos)
        logger.debug(msg)

        if int(mouseEvent.buttons()) != QtCore.Qt.NoButton:
            #user is dragging
            button = 'None'
            mod = 'None'

            self._ii_key_modifier = QtGui.QApplication.keyboardModifiers()

            # un-projecting to plot interaction on near plane

            # new way of using functions handles with event dictionary
            # if self._ii_key_modifier in self._event_dictionary.keys():
            #    self.self._event_dictionary[self._ii_key_modifier]

            # Left mouse button
            if self._ii_mouse_button == QtCore.Qt.LeftButton:
                button = 'Left'

                msg = "World coordinates: {:.4f}x {:.4f}y {:.1f}z".format(
                    *self.ii_coord)
                logger.debug(msg)

                # see keyPressEvent to set modifiers
                # if self._ii_key_modifier == QtCore.Qt.NoModifier:
                # behavior when no key are pressed
                if self.ii_tool == 'line':
                    self._ii_line_coords[1, :] = np.array(self.ii_coord)
                    mod = 'None'
                    button = 'Mouse_Left'
                    self._ii_line = True

                if self.ii_tool == 'hands_free':
                    self._ii_hands_free_line_coords = np.vstack(
                        (self._ii_hands_free_line_coords,
                         np.array(self.ii_coord)))

                    mod = 'None'
                    button = 'Mouse_Left'
                    self._ii_hands_free_line = True

                if self.ii_tool == 'point':
                    self._ii_point_coord = np.array(
                        self.ii_coord).reshape(1, -1)
                    self._ii_project_entity = \
                        glutils.project_vertices_qt(self,
                                                    self._ii_point_coord)
                    self._ii_unproject_entity = self._ii_point_coord
                    self._ii_point = True
                    self.nearest_to()

#                 if self._ii_key_modifier == QtCore.Qt.ShiftModifier:
#                     self.ii_tool = 'point'
#                     self._ii_point_coord = np.array(
#                         self.ii_coord).reshape(1, -1)
#                     self._ii_point = True

                self.update()

            elif self._ii_mouse_button == QtCore.Qt.RightButton:
                button = 'right'

            elif self._ii_key_modifier == QtCore.Qt.ControlModifier:

                mod = 'Control'

            elif self._ii_key_modifier == QtCore.Qt.AltModifier:

                mod = 'Alt'

            elif self._ii_key_modifier == (QtCore.Qt.AltModifier |
                                           QtCore.Qt.ControlModifier):

                mod = 'Alt+Control'

            elif self._ii_key_modifier == QtCore.Qt.ShiftModifier:

                mod = 'Shift'

            elif (self._ii_key_modifier == (QtCore.Qt.ControlModifier)):

                mod = 'Control'

                #dxdy = self._ii_mouse_pos - self._mi_oldcoord
                # self.orbit(dxdy)

            msg = "Modifier:{} Button:{}".format(mod, button)
            logger.debug(msg)

    def mouseDoubleClickEvent(self, mouse_event):

        msg = "mouse double click"
        logger.info(msg)

        self.double_click = True

        self.reset_inter_items()

        x = mouse_event.x() * 1.
        y = mouse_event.y() * 1.
        # note this is the far plane, z = 0 is the near plane
        z = 0 + 1. / (2 ** 10)
        msg = "ViewPort Coordinates: {:.2f}x {:.2f}y {:.2f}z".format(x, y, z)
        logger.info(msg)
        coord = glutils.unproject(
            x, y, z, self.modelview_mat, self.proj_mat, self.viewport)
        logger.info(coord)
        msg = "World Coordinates: {:.4f}x {:.4f}y {:.1f}z".format(*coord)
        logger.info(msg)
        self._ii_line_coords[0, :] = np.array(coord)
        self._ii_hands_free_line_coords = np.array(coord)

    def mousePressEvent(self, mouse_event):

        msg = "mouse press"
        logger.debug(msg)

        self.isPressed = True

        self.reset_inter_items()

        x = mouse_event.x() * 1.
        y = mouse_event.y() * 1.
        # note this is the far plane, z = 0 is the near plane
        z = 0 + 1. / (2 ** 10)
        msg = "ViewPort Coordinates: {:.2f}x {:.2f}y {:.2f}z".format(x, y, z)
        logger.info(msg)
        coord = glutils.unproject(
            x, y, z, self.modelview_mat, self.proj_mat, self.viewport)
        logger.info(coord)
        msg = "World Coordinates: {:.4f}x {:.4f}y {:.1f}z".format(*coord)
        logger.info(msg)
        self._ii_line_coords[0, :] = np.array(coord)
        self._ii_hands_free_line_coords = np.array(coord)

        if len(self.vertex_array) > 1:

            self._ii_project_data =\
                glutils.project_vertices_qt(self,
                                            self.vertex_array)
            msg = 'Projections Ready'
            logger.info(msg)

    def mouseReleaseEvent(self, e):
        #logger.info("mouse release")
        self.isPressed = False

        if self._ii_point:
            self._ii_project_entity = glutils.project_vertices_qt(self,
                                                                  self._ii_point_coord)
            self._ii_unproject_entity = self._ii_point_coord

        if self._ii_hands_free_line:
            self._ii_project_entity = \
                glutils.project_vertices_qt(self,
                                            self._ii_hands_free_line_coords)
            self._ii_unproject_entity = self._ii_hands_free_line_coords
        if self._ii_line:
            self._ii_project_entity = glutils.project_vertices_qt(self,
                                                                  self._ii_line_coords)
            self._ii_unproject_entity = self._ii_line_coords
        self.reset_inter_items()

        self.update()

    def points_in_region(self):

        indxs = np.ones(self.vertex_array.shape[0], dtype=np.bool)

        if not self.ii_tool == 'hands_free':
            self._ii_points_in_region = np.array([False])
            return

        vertices = self._ii_project_entity
        p = Path(vertices)

        indxs = indxs | (self.vertex_array[:, 0] < vertices.max(0)[0]) |\
            (self.vertex_array[:, 1] < vertices.max(0)[1]) |\
            (self.vertex_array[:, 0] > vertices.min(0)[0]) |\
            (self.vertex_array[:, 1] > vertices.min(0)[0])

        iindxs = p.contains_points(self._ii_project_data[indxs, :])
        indxs[indxs] = iindxs

        self._ii_points_in_region = indxs

    def nearest_to(self):
        point = self._ii_project_entity
        kdtree = spsp.cKDTree(self._ii_project_data)
        nearest = kdtree.query(point)
        self._ii_nearest = nearest
        msg = 'Nearest to {} is {}'.format(self._ii_project_entity, nearest)
        logger.info(msg)

    # Properties -------------------------------------------------------------

    @property
    def ii_mouse_pos(self):
        return self._ii_mouse_pos

    @ii_mouse_pos.setter
    def ii_mouse_pos(self, xy):
        xy = np.array(xy)
        self._ii_mouse_delta = xy - self._ii_mouse_pos
        self._ii_mouse_pos = xy

    @property
    def ii_mouse_delta(self):
        return self._ii_mouse_delta

    @property
    def ii_coord(self):
        #: returns projected coordinates on near plane
        z = 0 + 1. / (2 ** 10)
        coords = list(glutils.unproject(
            self.ii_mouse_pos[0], self.ii_mouse_pos[1], z, self.modelview_mat, self.proj_mat, self.viewport))

        return tuple(coords)

    @property
    def ii_tool(self):
        return self._ii_tool

    @ii_tool.setter
    def ii_tool(self, Input):
        if str(Input) not in INTERACTIVE_TOOLS:
            msg = "Tool must be one between {}".format(INTERACTIVE_TOOLS)
            logger.warning(msg)
            return
        self._ii_tool = str(Input)
        self.update()

    @property
    def ii_points_in_region(self):
        self.points_in_region()
        return self._ii_points_in_region

    #@property
    # def ii_unproject_entity(self):
    #    return self._ii_unproject_entity


class CanvasCamera(CanvasInteractive):

    def __init__(self, parent=None):

        super(CanvasCamera, self).__init__(parent)

        self._ca_perspective = True
        self._ca_ortho = False
        self._ca_dimensions = 3
        self._ca_plane = 'xy'
        self._ca_lock_xy_plane = False
        self._ca_viewport = None
        self._ca_center = np.zeros(3, dtype=np.float32)
        self._ca_azimuth = 45
        self._ca_fov = 90
        self._ca_elevation = 45
        self._ca_distance = 5
        self.ca_xzoom = 1
        self.ca_yzoom = 1
        self.ca_zzoom = 1
        self._ca_ortho_clip_planes = (0, 1, -1, 1)

        self.setMouseTracking(True)
        self.grabGesture(QtCore.Qt.PinchGesture)
        self.grabGesture(QtCore.Qt.TapGesture)
        self.grabGesture(QtCore.Qt.PanGesture)
        self.grabGesture(QtCore.Qt.SwipeGesture)
        self.grabGesture(QtCore.Qt.TapAndHoldGesture)

    def pre_paintGL(self):
        '''
        This gets executed before rendering of the object, perfect for camera
        '''
        super(CanvasCamera, self).pre_paintGL()

        self.setProjection()
        self.setModelview()

    # MouseInteraction -------------------------------------------------------

    def mouseMoveEvent(self, mouseEvent):
        super(CanvasCamera, self).mouseMoveEvent(mouseEvent)

        if int(mouseEvent.buttons()) != QtCore.Qt.NoButton:

            #xy = self._ii_mouse_pos
            dxdy = self._ii_mouse_delta

            if int(mouseEvent.buttons()) & QtCore.Qt.RightButton:
                #button = 'right'

                if self._ii_key_modifier == QtCore.Qt.ControlModifier:

                    if self._ca_lock_xy_plane:
                        self.ca_xzoom += dxdy[0] * 1. / 100
                        self.ca_yzoom -= dxdy[1] * 1. / 100

                elif self._ii_key_modifier == QtCore.Qt.AltModifier:

                    self.pan(dxdy[0] * -1 * 1. / 100, dxdy[1] * 1. / 100, 0)

                else:

                    if self._ca_lock_xy_plane:
                        self.ca_xzoom += dxdy[0] * 1. / 100
                        self.ca_yzoom -= dxdy[1] * 1. / 100

                    else:
                        self.orbit(dxdy)

            msg = "mouseMoveEvent(): pos:{} dxdy:{}".format(
                self._ii_mouse_pos, self._ii_mouse_delta)
            logger.debug(msg)

    def wheelEvent(self, wheel_event):

        center = self.ca_center

        if (wheel_event.modifiers() & QtCore.Qt.ShiftModifier):

            self.ca_distance *= 0.999 ** wheel_event.delta()

        elif wheel_event.orientation() == QtCore.Qt.Horizontal:
            center = (
                center[0] + wheel_event.delta() * -1 * 1. / 100, center[1], center[2])
            self.ca_center = center
        elif wheel_event.orientation() == QtCore.Qt.Vertical:
            center = (
                center[0], center[1] + wheel_event.delta() * -1 * 1. / 100, center[2])
            self.ca_center = center

#     def grabGesture(self, gesture):
#         super(CanvasCamera, self).grabGesture(gesture)
#         logger.info(gesture)
    def event(self, qevent):

        if isinstance(qevent, QtGui.QGestureEvent):
            return self.gestureEvent(qevent)

        return super(CanvasCamera, self).event(qevent)

    def gestureEvent(self, gesture_event):
        # pdb.set_trace()
        for gesture in gesture_event.gestures():

            if isinstance(gesture, QtGui.QPinchGesture):
                #logger.info('rangle {}'.format(gesture.rotationAngle()))
                #logger.info('sfactor {}'.format(gesture.scaleFactor()))
                #logger.info('lrangle {}'.format(gesture.lastRotationAngle()))
                #logger.info('lsfactor {}'.format(gesture.lastScaleFactor()))
                scale = gesture.scaleFactor() - gesture.lastScaleFactor()
                self.ca_xzoom += scale
                self.ca_yzoom += scale
                self.ca_zzoom += scale
                #logger.info((scale, self.ca_xzoom, self.ca_yzoom))

            if isinstance(gesture, QtGui.QPanGesture):

                center = self.ca_center
                offset = gesture.offset() - gesture.lastOffset()
                dx = offset.x()
                dy = offset.y()
                center_x = center[0] + dx / self.ca_xzoom * -1 * 1. / 100
                center_y = center[1] + dy / self.ca_yzoom * -1 * 1. / 100

                self.ca_center = (center_x, center_y, center[2])

                # logger.info(offset)
                #logger.info('rangle {}'.format(gesture.rotationAngle()))
                #logger.info('sfactor {}'.format(gesture.scaleFactor()))
                #logger.info('lrangle {}'.format(gesture.lastRotationAngle()))
                #logger.info('lsfactor {}'.format(gesture.lastScaleFactor()))
                #scale = gesture.scaleFactor() - gesture.lastScaleFactor()
                #self.ca_xzoom += scale
                #self.ca_yzoom += scale
                #logger.info((scale, self.ca_xzoom, self.ca_yzoom))

        return True

    def setProjection(self, region=None):

        m = self.projectionMatrix(region)
        gl.glMatrixMode(gl.GL_PROJECTION)
        gl.glLoadIdentity()
        a = np.array(m.copyDataTo()).reshape((4, 4))
        gl.glMultMatrixf(a.transpose())

    def projectionMatrix(self, region=None):

        if region is None:
            region = (0, 0, self.width(), self.height())

        if self.ca_perspective:
            x0, y0, w, h = self.ca_viewport
            dist = self.ca_distance
            fov = self.ca_fov
            nearClip = dist * 0.001
            farClip = dist * 1000.

            r = nearClip * np.tan(fov * 0.5 * np.pi / 180.)
            t = r * h / w

            # convert screen coordinates (region) to normalized device coordinates
            # Xnd = (Xw - X0) * 2/width - 1
            # Note that X0 and width in these equations must be the values used in
            # viewport
            left = r * ((region[0] - x0) * (2.0 / w) - 1)
            right = r * ((region[0] + region[2] - x0) * (2.0 / w) - 1)
            bottom = t * ((region[1] - y0) * (2.0 / h) - 1)
            top = t * ((region[1] + region[3] - y0) * (2.0 / h) - 1)

            tr = QtGui.QMatrix4x4()
            tr.frustum(left, right, bottom, top, nearClip, farClip)
            return tr

        if self.ca_ortho:

            x0, y0, w, h = self.ca_viewport
            dist = self.ca_distance
            fov = self.ca_fov
            nearClip = dist * 0.001
            farClip = dist * 1000.

            r = nearClip * np.tan(fov * 0.5 * np.pi / 180.)
            t = r * h / w

            # convert screen coordinates (region) to normalized device coordinates
            # Xnd = (Xw - X0) * 2/width - 1
            # Note that X0 and width in these equations must be the values used in
            # viewport
            left = r * ((region[0] - x0) * (2.0 / w) - 1)\
                - (self.ca_distance) * np.tan(fov * 0.5 *
                                              np.pi / 180.)
            right = r * ((region[0] + region[2] - x0) * (2.0 / w) - 1)\
                + (self.ca_distance) * np.tan(fov * 0.5 *
                                              np.pi / 180.)
            bottom = t * ((region[1] - y0) * (2.0 / h) - 1)\
                - (self.ca_distance) * np.tan(fov * 0.5 *
                                              np.pi / 180.)
            top = t * ((region[1] + region[3] - y0) * (2.0 / h) - 1)\
                + (self.ca_distance) * np.tan(fov * 0.5 *
                                              np.pi / 180.)
            tr = QtGui.QMatrix4x4()

            tr.ortho(left, right, bottom, top, nearClip, farClip)
            if self._ca_lock_xy_plane:
                tr.ortho(*(self._ca_ortho_clip_planes + (nearClip, farClip)))
            return tr

    def setModelview(self):
        gl.glMatrixMode(gl.GL_MODELVIEW)
        gl.glLoadIdentity()
        m = self.viewMatrix()
        a = np.array(m.copyDataTo()).reshape((4, 4))
        gl.glMultMatrixf(a.transpose())

    def viewMatrix(self):
        tr = QtGui.QMatrix4x4()
        tr.translate(0.0, 0.0, -self.ca_distance)
        tr.rotate(self.ca_elevation - 90, 1, 0, 0)
        tr.rotate(self.ca_azimuth + 180, 0, 0, 1)

        tr.scale(self.ca_xzoom, self.ca_yzoom, self.ca_zzoom)
        center = self.ca_center
        tr.translate(-center[0], -center[1], -center[2])
        return tr

    def orbit(self, dxdy):
        """Orbits the Camera around the center position. *azim* and *elev* are given in degrees."""
        msg = "orbit(): {}".format(dxdy)
        logger.debug(msg)
        self._ca_azimuth += dxdy[0]
        # self.opts['elevation'] += elev
        self.ca_elevation += dxdy[1]
        # np.clip(
        #    self.ca_elevation + dxdy[1], -90, 90)

    def pan(self, dx, dy, dz, relative=False):
        """
        Moves the center (look-at) position while holding the Camera in place.

        If relative=True, then the coordinates are interpreted such that x
        if in the global xy plane and points to the right side of the view, y is
        in the global xy plane and orthogonal to x, and z points in the global z
        direction. Distances are scaled roughly such that a value of 1.0 moves
        by one pixel on screen.

        """
        if not relative:
            self._ca_center += np.array([dx, dy, dz])
        else:
            cPos = self.cameraPosition()
            cVec = self._ca_center - cPos
            dist = cVec.length()  # distance from Camera to center
            # approx. width of view at distance of center point
            xDist = dist * 2. * np.tan(0.5 * self._ca_fov * np.pi / 180.)
            xScale = xDist / self.width()
            zVec = QtGui.QVector3D(0, 0, 1)
            xVec = QtGui.QVector3D.crossProduct(zVec, cVec).normalized()
            yVec = QtGui.QVector3D.crossProduct(xVec, zVec).normalized()
            self._ca_center = self._ca_center + xVec * \
                xScale * dx + yVec * xScale * dy + zVec * xScale * dz
        self.update()

    def zoom(self, dxdy):
        gl.glMatrixMode(gl.GL_MODELVIEW)

    def resizeGL(self, widthInPixels, heightInPixels):

        self.setProjection()
        self.setModelview()

    # Properties -------------------------------------------------------------

    @property
    def ca_xzoom(self):
        return self._ca_xzoom

    @ca_xzoom.setter
    def ca_xzoom(self, Input):
        if Input >= 0:
            self._ca_xzoom = Input
            self.update()

    @property
    def ca_yzoom(self):
        return self._ca_yzoom

    @ca_yzoom.setter
    def ca_yzoom(self, Input):
        if Input >= 0:
            self._ca_yzoom = Input
            self.update()

    @property
    def ca_zzoom(self):
        return self._ca_zzoom

    @ca_zzoom.setter
    def ca_zzoom(self, Input):
        if Input >= 0:
            self._ca_zzoom = Input
            self.update()

    @property
    def ca_plane(self):
        return self._ca_plane

    @ca_plane.setter
    def ca_plane(self, Input):
        if Input == 'xy' or Input == 'yx':
            self._ca_azimuth = 180
            self._ca_elevation = 90
        if Input == 'xz' or Input == 'zx':
            self._ca_azimuth = -90
            self._ca_elevation = 0
        if Input == 'yz' or Input == 'zy':
            self._ca_azimuth = 0
            self._ca_elevation = 0

        self._ca_plane = Input
        self.ca_ortho = True

    @property
    def ca_viewport(self):

        self._ca_viewport = (0, 0, self.width(), self.height())
        gl.glViewport(*self._ca_viewport)
        return self._ca_viewport

    @ca_viewport.setter
    def ca_viewport(self, Input):
        self._ca_viewport = Input
        self.update()

    @property
    def ca_azimuth(self):
        return self._ca_azimuth

    @ca_azimuth.setter
    def ca_azimuth(self, Input):
        self._ca_azimuth = Input
        self.update()

    @property
    def ca_distance(self):
        return self._ca_distance

    @ca_distance.setter
    def ca_distance(self, Input):
        self._ca_distance = Input
        self.update()

    @property
    def ca_fov(self):
        return self._ca_fov

    @ca_fov.setter
    def ca_fov(self, Input):
        if Input < 180:
            self._ca_fov = Input
            self.update()

    @property
    def ca_center(self):
        return self._ca_center

    @ca_center.setter
    def ca_center(self, Input):
        self._ca_center = np.array(Input)
        self.update()

    @property
    def ca_perspective(self):
        return self._ca_perspective

    @ca_perspective.setter
    def ca_perspective(self, Input):
        self._ca_perspective = Input

        if Input:
            self._ca_ortho = False
        else:
            self._ca_ortho = True

        self.update()

    @property
    def ca_elevation(self):
        return self._ca_elevation

    @ca_elevation.setter
    def ca_elevation(self, Input):
        self._ca_elevation = Input
        self.update()

    @property
    def ca_ortho(self):
        return self._ca_ortho

    @ca_ortho.setter
    def ca_ortho(self, Input):
        self._ca_ortho = Input
        if Input:
            self._ca_perspective = False
        else:
            self._ca_perspective = True
        self.update()


class CanvasCameraItems(CanvasCamera, CanvasGraphicItems):

    def __init__(self, parent=None):
        super(CanvasCameraItems, self).__init__(parent)


if __name__ == '__main__':

    logging.basicConfig()
    logger = logging.getLogger()
    logger.setLevel('INFO')

    if not QtGui.QApplication.instance():
        app = QtGui.QApplication(sys.argv)
    else:
        app = QtGui.QApplication.instance()

    canvas = CanvasCameraItems()
    data = [(0, 0, 0),
            (1, 0, 0),
            (1, 1, 0),
            (0, 1, 0),
            (0, 0, 0),
            (0, 0, 1),
            (1, 0, 1),
            (1, 0, 0),
            (1, 0, 1),
            (1, 1, 1),
            (1, 1, 0),
            (1, 1, 1),
            (0, 1, 1),
            (0, 1, 0),
            (0, 1, 1),
            (0, 0, 1)]
    #data = np.array(data).astype('i')
    #canvas.vertex_array = data
    canvas.vertex_array = np.random.randn(1000000, 3)
    canvas.show()
    import rlcompleter
    import readline
    readline.parse_and_bind('tab:complete')
