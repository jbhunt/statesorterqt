from PyQt4.QtGui import *
from PyQt4.QtCore import *
from qrangeslider import QRangeSlider
import numpy as np
from statesorterqt.datavis import _plot_scatterplot, _plot_spectrogram_against_score, _plot_hypnogram, _plot_spectral_trajectories
from statesorterqt.utils import _remove_axis_frame

def addScatterPlotControllerTab(_self):
    
    """
    builds a UI for playing with the scatterplot.
    """
    
    #_self.checkContext()
    
    def buildController():
        
        """
        """
        
        alphabet = np.array(list(map(chr, range(97, 123))))
        _self.cb_sp_chid = QComboBox()
        #_self.cb_sp_chid.setFixedWidth(50)
        n = _self._n_channels
        for i in range(n):
            _self.cb_sp_chid.addItem(alphabet[i])
        cb1_label = QLabel('channel:')
        cb1_label.setFixedWidth(50)
        cb1_label.setToolTip('select a channel.')
    
        _self.le_sp_s = QLineEdit()
        #_self.le_sp_s.setFixedWidth(50)
        _self.le_sp_s.setText('1')
        le1_label = QLabel('markersize:')
        le1_label.setFixedWidth(50)
        le1_label.setToolTip('please select marker size (in pts).')
        
        _self.cb_sp_mode = QComboBox()
        #_self.cb_sp_mode.setFixedWidth(50)
        for mode in ['density', 'velocity', 'delta', 'theta', 'alpha', 'beta', 'gamma','WT']:
            _self.cb_sp_mode.addItem(mode)
        cb2_label = QLabel('mode:')
        cb2_label.setFixedWidth(50)
        cb2_label.setToolTip('select which band-mapping to use.')
        
        _self.cb_sp_cmap = QComboBox()
        #_self.cb_sp_cmap.setFixedWidth(50)
        for cmap in _self._colormaps:
            _self.cb_sp_cmap.addItem(cmap)
        cb3_label = QLabel('cmap:')
        cb3_label.setFixedWidth(50)
        cb3_label.setToolTip('select a coloromap.')
        
        plotButton = QPushButton('plot')
        plotButton.clicked.connect(plotPushed)
        
        _self.rs_sp_vrange = QRangeSlider()
        _self.rs_sp_vrange.setFixedSize(QSize(100, 20))
        _self.rs_sp_vrange.setRange(0,100)
        _self.rs_sp_vrange.setMin(0)
        _self.rs_sp_vrange.setMax(100)
        _self.rs_sp_vrange.setDrawValues(False)
        rs1_label = QLabel('v-range:')
        rs1_label.setFixedWidth(50)
        rs1_label.setToolTip('vmin and vmax values (as percentiles of the distribution of spectral amplitude in the Sxx) for normalization.')
        
        col1, col1_child = QVBoxLayout(), QGridLayout()
        
        col1_child.addWidget(cb1_label, 0, 0)
        col1_child.addWidget(cb2_label, 1, 0)
        col1_child.addWidget(cb3_label, 2, 0)
        
        col1_child.addWidget(_self.cb_sp_chid, 0, 1)
        col1_child.addWidget(_self.cb_sp_mode, 1, 1)
        col1_child.addWidget(_self.cb_sp_cmap, 2, 1)
        
        col1_child.addWidget(rs1_label, 0, 3)
        col1_child.addWidget(_self.rs_sp_vrange, 0, 4)
        
        col1_child.addWidget(plotButton, 5, 0, 1, 2)
        
        col1.addLayout(col1_child)
        col1.addStretch()
        
        controllerLayout = QHBoxLayout()
        controller = QWidget()
        controllerLayout.addLayout(col1)
        controllerLayout.addStretch()
        controller.setLayout(controllerLayout)
        
        return controller
        
    def plotPushed():
        
        """
        """
        
        _self.mainTabWidget.setCurrentIndex(1)
        
        fig = _self.mplCanvas.figure
        fig.clf()
        ax = fig.add_subplot(111)
        
        alphabet = np.array(list(map(chr, range(97, 123))))
        ch_idx = int(np.where(alphabet==_self.cb_sp_chid.currentText())[0])
        Sxx = _self._all_spectra[ch_idx]
        markersize = float(_self.le_sp_s.text())
        mode = str(_self.cb_sp_mode.currentText())
        cmap = str(_self.cb_sp_cmap.currentText())
        p1p2 = _self.rs_sp_vrange.getRange()
        _plot_scatterplot(ax, _self._xy, Sxx, _self._z, _self._vel, mode, markersize, cmap, p1p2)
        # _plot_scatterplot(ax, _self._xy, Sxx, _self._z, _self._vel, mode, markersize, cmap, p1p2)
        _remove_axis_frame(ax, remove_ticks=True)
        fig.tight_layout()
        fig.canvas.draw()
    
    if hasattr(_self, '_tab_sp_id') is False:
        controller = buildController()
        _self._tab_sp_id = _self.controllerTabWidget.addTab(controller, 'scatterplot')
        _self.update()
    
def addSpectrogramControllerTab(_self):
        
    """
    """
    
    def buildController():

        all_spectra = _self._all_spectra
        n_channels = _self._n_channels
        n_seconds = int(all_spectra[0].shape[1])
        alphabet = np.array(list(map(chr, range(97, 123))))
        
        _self.sg_ch = QComboBox()
        for i in range(n_channels):
            _self.sg_ch.addItem(alphabet[i])
        sg_ch_label = QLabel('channel:')
        sg_ch_label.setToolTip('select a channel.')
        
        _self.sg_cmap = QComboBox()
        for cmap in _self._colormaps:
            _self.sg_cmap.addItem(cmap)
        sg_cmap_label = QLabel('cmap:')
        sg_cmap_label.setToolTip('select a colormap for the spectrogram.')
        
        _self.sg_hp = QLineEdit()
        _self.sg_hp.setText('0.5')
        _self.sg_hp.setFixedWidth(30)
        sg_hp_label = QLabel('high-pass:')
        sg_hp_label.setToolTip('high-pass (Hz) for the filter.')

        _self.sg_lp = QLineEdit()
        _self.sg_lp.setText('100')
        _self.sg_lp.setFixedWidth(30)
        sg_lp_label = QLabel('low-pass:')
        sg_lp_label.setToolTip('low-pass (Hz) for the filter.')
        
        _self.sg_rs1= QRangeSlider()
        _self.sg_rs1.setFixedSize(QSize(100, 20))
        _self.sg_rs1.setRange(0, n_seconds)
        _self.sg_rs1.setMin(0)
        _self.sg_rs1.setMax(n_seconds)
        _self.sg_rs1.setDrawValues(False)
        sg_rs1_label = QLabel('window:')
        sg_rs1_label.setToolTip('time window range slider (in seconds).')
        
        _self.sg_rs2 = QRangeSlider()
        _self.sg_rs2.setFixedSize(QSize(100, 20))
        _self.sg_rs2.setRange(0,100)
        _self.sg_rs2.setMin(0)
        _self.sg_rs2.setMax(100)
        _self.sg_rs2.setDrawValues(False)
        sg_rs2_label = QLabel('v-range:')
        sg_rs2_label.setToolTip('vmin and vmax for normalizing spectral amplitudes.')
        
        plotButton = QPushButton('plot')
        plotButton.clicked.connect(plotPushed)
        
        col1, col1_child = QVBoxLayout(), QGridLayout()
        col1_child.addWidget(sg_ch_label, 0, 0)
        col1_child.addWidget(_self.sg_ch, 0, 1)
        col1_child.addWidget(sg_hp_label, 1, 0)
        col1_child.addWidget(_self.sg_hp, 1, 1)
        col1_child.addWidget(sg_lp_label, 2, 0)
        col1_child.addWidget(_self.sg_lp, 2, 1)
        col1_child.addWidget(sg_rs1_label, 0, 2)
        col1_child.addWidget(_self.sg_rs1, 0, 3)
        col1_child.addWidget(sg_rs2_label, 1, 2)
        col1_child.addWidget(_self.sg_rs2, 1, 3)
        col1_child.addWidget(sg_cmap_label, 2, 2)
        col1_child.addWidget(_self.sg_cmap, 2, 3)
        col1_child.addWidget(plotButton, 3, 0, 1, 2)
        
        col1.addLayout(col1_child)
        col1.addStretch()
        controllerLayout = QHBoxLayout()
        controllerLayout.addLayout(col1)
        controllerLayout.addStretch()
        controller = QWidget()
        controller.setLayout(controllerLayout)
        return controller
        
    def plotPushed():
        
        """
        """
        
        _self.mainTabWidget.setCurrentIndex(1)
        
        alphabet = np.array(list(map(chr, range(97, 123))))
        ch_idx = int(np.where(alphabet==_self.sg_ch.currentText())[0])
        sxx = _self._all_spectra[ch_idx]
        
        hp, lp = float(_self.sg_hp.text()), float(_self.sg_lp.text())
        bandwidth = (hp, lp)
        window = _self.sg_rs1.getRange()
        p1p2 = _self.sg_rs2.getRange()
        cmap = str(_self.sg_cmap.currentText())
        
        fig = _self.mplCanvas.figure
        fig.clf()
        
        _plot_spectrogram_against_score(fig, sxx, _self._smooth_score, bandwidth, window, p1p2, cmap)
        
        for ax in fig.axes:
            _remove_axis_frame(ax, remove_ticks=True)
        fig.tight_layout()
        fig.subplots_adjust(hspace=0)
        fig.canvas.draw()
    
    # prevents the tab from being added twice if the build function is called again.
    if hasattr(_self, '_tab_sg_id') is False:
        controller = buildController()
        _self._tab_sg_id = _self.controllerTabWidget.addTab(controller, 'spectrogram')
        _self.update()
    
def addMainControllerTab(_self):
    
    """
    """
    
    def buildController():
        
        """
        """
    
        col1, col1_child = QVBoxLayout(), QGridLayout()
        controllerLayout = QHBoxLayout()
        
        _self.cb_wake, _self.cb_nrem, _self.cb_rem = QComboBox(), QComboBox(), QComboBox()
        state_labels = ['wake:', 'nrem:', 'rem:']
        
        for i, cb in enumerate([_self.cb_wake, _self.cb_nrem, _self.cb_rem]):
            #cb.setFixedWidth(50)
            for color in ['blue', 'orange', 'green']:
                cb.addItem(color)
            label = QLabel(state_labels[i])
            label.setFixedWidth(50)
            col1_child.addWidget(label, i, 0)
            col1_child.addWidget(cb, i, 1)
            
        processButton = QPushButton('process sorting')
        processButton.clicked.connect(_self.processStateSorting)
        col1_child.addWidget(processButton, i+1, 0, 1, 2)
        
        col1.addLayout(col1_child)
        col1.addStretch()
        controllerLayout.addLayout(col1)
        
        _self.cb_cmap = QComboBox()
        for cmap in _self._colormaps:
            _self.cb_cmap.addItem(cmap)
        cb_cmap_label = QLabel('color map:')
        cb_cmap_label.setFixedWidth(50)
        
        filterButton = QPushButton('toggle filter')
        filterButton.setShortcut('Ctrl+F')
        filterButton.clicked.connect(plotPushed)
        
        _self.rs_ss_vrange = rs = QRangeSlider()
        rs.setRange(0, 100)
        rs.setMin(0)
        rs.setMax(100)
        rs.setDrawValues(False)
        rs.setFixedHeight(20)
        
        rs_label = QLabel('v-range (below):')
        rs_label.setFixedHeight(20)
        
        col2, col2_child = QVBoxLayout(), QGridLayout()
        col2_child.addWidget(cb_cmap_label, 0, 0)
        col2_child.addWidget(_self.cb_cmap, 0, 1)
        col2_child.addWidget(rs_label, 1, 0, 1, 2)
        col2_child.addWidget(rs, 2, 0, 1, 2)
        col2_child.addWidget(filterButton, 3, 0, 1, 2)
        
        col2.addLayout(col2_child)
        col2.addStretch()
        controllerLayout.addLayout(col2)
        
        controllerLayout.addStretch()
        controller = QWidget()
        controller.setLayout(controllerLayout)
        return controller
    
    def plotPushed():
        
        """
        """
        
        p1p2 = _self.rs_ss_vrange.getRange()
        _self.stateSorter.toggleDensityFilter(p1p2)
    
    if hasattr(_self, '_tab_main_id') is False:
        controller = buildController()
        _self._tab_main_id = _self.controllerTabWidget.addTab(controller, 'main')
        _self.update()
    
def addHypnogramTab(_self):
    
    """
    """
    
    def buildController():
        
        """
        """
        
        _self.hg_nhrs = QLineEdit()
        _self.hg_nhrs.setText('0.11')
        _self.hg_nhrs.setFixedWidth(50)
        hg_nhrs_label = QLabel('n-hours:')
        hg_nhrs_label.setFixedWidth(50)
        hg_nhrs_label.setToolTip('input the number of hours per column.')
        
        plotButton = QPushButton('plot')
        plotButton.clicked.connect(plotPushed)
        
        col1, col1_child = QVBoxLayout(), QGridLayout()
        col1_child.addWidget(hg_nhrs_label, 0, 0)
        col1_child.addWidget(_self.hg_nhrs, 0, 1)
        col1_child.addWidget(plotButton, 1, 0, 1, 2)
        col1.addLayout(col1_child)
        col1.addStretch()
        
        controllerLayout = QHBoxLayout()
        controllerLayout.addLayout(col1)
        controllerLayout.addStretch()
        controller = QWidget()
        controller.setLayout(controllerLayout)
        
        return controller
        
    def plotPushed():
        
        _self.mainTabWidget.setCurrentIndex(1)
        
        n_hours = float(_self.hg_nhrs.text())
        
        fig = _self.mplCanvas.figure
        fig.clf()
        ax = fig.add_subplot(111)
        
        _plot_hypnogram(ax, _self._smooth_score, n_hours)
        
        _remove_axis_frame(ax, remove_ticks=True)
        fig.tight_layout()
        fig.canvas.draw()
        
    if hasattr(_self, '_tab_hg_id') is False:
        controller = buildController()
        _self._tab_hg_id = _self.controllerTabWidget.addTab(controller, 'hypnogram')
        _self.update()
        
def addStateTransitionsTab(_self):
    
    """
    """
    
    def buildController():
        
        """
        """
        
        # home- and destination-state
        state_names = ['wake', 'nrem', 'rem']
        _self.sts_home = QComboBox()
        _self.sts_dest = QComboBox()
        for i in range(3):
            _self.sts_home.addItem(state_names[i])
            _self.sts_dest.addItem(state_names[i])
        
        sts_home_label = QLabel('home-state:')
        sts_home_label.setToolTip('select the home state.')
        sts_dest_label = QLabel('destination-state:')
        sts_dest_label.setToolTip('select the destination state.')
        
        # max time in transit
        _self.sts_ttrans = QLineEdit()
        _self.sts_ttrans.setText('60')
        _self.sts_ttrans.setFixedWidth(50)
        sts_ttrans_label = QLabel('max. travel time:')
        sts_ttrans_label.setToolTip('input the maximum time a trajectory may spend in transit between states to be considered valid.')
        
        # min time in-state pre- and post- transition.
        _self.sts_tstatic = QLineEdit()
        _self.sts_tstatic.setText('15')
        _self.sts_tstatic.setFixedWidth(50)
        sts_tstatic_label = QLabel('min. time static:')
        sts_tstatic_label.setToolTip('input the criteria for minimum time spent in-state prior to, and following transition.')
        
        # cmap
        _self.sts_cmap = QComboBox()
        for cmap in _self._colormaps:
            _self.sts_cmap.addItem(cmap)
        _self.sts_cmap.addItem('native')
        sts_cmap_label = QLabel('color map:')
        #sts_cmap_label.setFixedWidth(50)
        
        # plot button
        plotButton = QPushButton('plot')
        plotButton.clicked.connect(plotPushed)
        
        col1, col1_child = QVBoxLayout(), QGridLayout()
        col1_child.addWidget(sts_home_label, 0, 0)
        col1_child.addWidget(_self.sts_home, 0, 1)
        col1_child.addWidget(sts_dest_label, 1, 0)
        col1_child.addWidget(_self.sts_dest, 1, 1)
        col1_child.addWidget(sts_ttrans_label, 2, 0)
        col1_child.addWidget(_self.sts_ttrans, 2, 1)
        col1_child.addWidget(sts_tstatic_label, 3, 0)
        col1_child.addWidget(_self.sts_tstatic, 3, 1)
        col1_child.addWidget(sts_cmap_label, 0, 2)
        col1_child.addWidget(_self.sts_cmap, 0, 3)
        col1_child.addWidget(plotButton, 4, 0, 1, 2)
        col1.addLayout(col1_child)
        col1.addStretch()
        
        controllerLayout = QHBoxLayout()
        controllerLayout.addLayout(col1)
        controllerLayout.addStretch()
        controller = QWidget()
        controller.setLayout(controllerLayout)
        
        return controller
        
    def plotPushed():
        
        """
        """
        
        _self.mainTabWidget.setCurrentIndex(1)
        
        home_id = _self._state_ids_dict[str(_self.sts_home.currentText())]
        dest_id = _self._state_ids_dict[str(_self.sts_dest.currentText())]
        ttrans, tstatic = int(_self.sts_ttrans.text()), int(_self.sts_tstatic.text())
        cmap = str(_self.sts_cmap.currentText())
        
        fig = _self.mplCanvas.figure
        fig.clf()
        ax = fig.add_subplot(111)
        
        _plot_spectral_trajectories(ax,
                                    _self._xy,
                                    home_id, dest_id,
                                    _self._smooth_score,
                                    _self._boundary_vertices,
                                    ttrans,
                                    tstatic,
                                    1,
                                    cmap)
        
        _remove_axis_frame(ax, remove_ticks=True)
        fig.tight_layout()
        fig.canvas.draw()
        
    if hasattr(_self, '_tab_sts_id') is False:
        controller = buildController()
        _self._tab_sts_id = _self.controllerTabWidget.addTab(controller, 'state trans.')
        _self.update()