'''
Created on Jan 3, 2018
@author: Josh Hunt
'''

from __future__ import division
from PyQt4.QtCore import *
from PyQt4.QtGui import *
import os
import sip
from qtconsole.rich_ipython_widget import RichJupyterWidget
from qtconsole.inprocess import QtInProcessKernelManager
from IPython.lib import guisupport
import numpy as np
import matplotlib as mpl
#from matplotlib.pylab import Figure
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from statesorterqt.glwidgets.GLCanvas import CanvasCameraItems
from statesorterqt.glwidgets import GLUtility as glutils

class glStateSorterWidget(CanvasCameraItems):
    
    """
    main widget for the state-sorting.
    """
    
    def __init__(self, parent=None):
        
        super(glStateSorterWidget, self).__init__(parent)
        self.initUI()
    
    def initUI(self):
        
        # basic canvas properties
        self.bc_pointsize = 3
        self.bc_background = (1., 1., 1., .7) #white background
        self._black = [0, 0, 0, 255]
        self._bc_default_color = self._black
        
        # higher-order canvas properties.
        self.ca_ortho = True
        self.ca_distance = 1.5
        self.ca_center = (-0.5, 0, -0.1)
        self.ca_yzoom = 3
        self.ca_plane = 'xy'
        self._ca_lock_xy_plane = True
        
        # miscellaneous properties.
        self.gi_axis_2D = True
        self.ii_tool = 'hands_free'
        self.glcw_cluster_density = 1
        
        # me-defined things.
        self._cluster_masks_raw = []
        self._cluster_masks_smooth = []
        self._z = None
        self._colors = None
        self._filter_on = False
        self._lock_interaction = False
        self.blueOrangeGreen = [[1, 119, 180, 255],
                                [255, 127, 14, 255],
                                [44, 160, 44, 255]]
        
    def updateUIPostProcessing(self, mainWindow):
        
        """
        sets the context of the stateSorter after the ns1 file is opened.
        
        :param mainWindow: this is the QMainWindow-type object for the app
        
        the mainWindow object can be used to access contextual information
        like the current text of a combo box or line edit object.
        """
        
        self._colors = np.tile(np.array(self._black), (self.vertex_array.shape[0], 1))
        self.bc_color_array = self._colors
        
        # (below) this is very important. It affords access to context
        # within the mainWindow. e.g. gives access to the combo boxes
        # and line edits in the widgets.
        
        self.mainWindow = mainWindow
           
    def wheelEvent(self, wheel_event):
        
        center = self.ca_center
        
        if (wheel_event.modifiers() & Qt.ShiftModifier):
            self.ca_distance *= 0.999 ** wheel_event.delta()
            
        elif (wheel_event.modifiers() & Qt.ControlModifier):
            
            if wheel_event.orientation() == Qt.Horizontal:
                center = (
                center[0] + wheel_event.delta() * -1 * 1. / 1000,
                center[1],
                center[2])
                self.ca_center = center
                
            elif wheel_event.orientation() == Qt.Vertical:
                center = (
                center[0],
                center[1] + wheel_event.delta() * 1 * 1. / 1000,
                center[2])
                self.ca_center = center
                
    def mouseReleaseEvent(self, event):
        
        if self._lock_interaction is False:
            
            self.isPressed = False
            self._ii_project_entity = glutils.project_vertices_qt(self,
                                                                  self._ii_hands_free_line_coords)
            self._ii_unproject_entity = self._ii_hands_free_line_coords
            self.reset_inter_items()
            self.storeClusterMasks()
            self.colorClusters()
        
    # TODO: figure out how the xzoom/yzoom affect the center posiiton.
    #def findCanvasCenter(self, xy):
    #    
    #    """
    #    returns the centroid for the xy data.
    #    
    #    :param xy: xy-points of shape (2, n_seconds).
    #    """
    #    
    #    self._centroid = centroid = np.mean(xy, axis=0)
    #    self.ca_center = centroid
            
    def reCenterCanvas(self):
        
        self.ca_center = (-0.5, 0, -0.1)
        self.ca_distance = 1
        # self.ca_xzoom = 1
        self.ca_yzoom = 2
        self.update()
        
    def undoCut(self):
        
        if self._lock_interaction is False:
            try:
                self._cluster_masks_raw.pop()
            except IndexError as err:
                print('no clusters sorted. nothing to pop.')
            self.colorClusters()
            
    def storeClusterMasks(self):
        
        """
        stores the masks of the cluster cuts. stores a maximum of three
        masks.
        """
        
        n_clusters_sorted = len(self._cluster_masks_raw)
        if n_clusters_sorted < 3:
            self._cluster_masks_raw.append(self.ii_points_in_region)
        
        # this only lets there be three clusters    
        else:
            self._cluster_masks_raw[-1] = self.ii_points_in_region
    
    # method functionality pushed to the colorClusters function.     
    #def uncolorClusters(self):
    #    
    #    """
    #    every time this is called re-colors all of the points with either
    #    the default color mapping (all points are black), or a custom
    #    color-mapping (as defined by self._colors).
    #    """
    #    
    #    mode = self._artist_mode
    #    
    #    # all points re-colored black
    #    if mode == 'default':
    #        self.bc_color_array = self._colors
    #     
    #    # all points re-colored with custom cmap.    
    #    elif mode == 'density':
    #        self.bc_color_array = self._colors
    
    def colorClusters(self, scoring_smoothed=False):
        
        """
        assigns colors to the points in the cut-outs of the clusters.
        
        :param bool scoring_smoother: if True will use the array which
            contains the smoothed scoring for the clusters instead of
            the raw scoring.
        
        """
        
        if scoring_smoothed is False:
            masks = self._cluster_masks_raw
        else:
            masks = self._cluster_masks_smooth
            
        if self._filter_on is False:
            self.bc_color_array = self._colors
            
            for i, mask in enumerate(masks):
                updated_bc_color_array = np.array(
                    self.bc_color_array, copy=True)
                updated_bc_color_array[mask] = self.blueOrangeGreen[i]
                self.bc_color_array = updated_bc_color_array
               
        if self._filter_on is True:
            self.bc_color_array = self._colors
            
            for mask in masks:
                updated_bc_color_array = np.array(
                    self.bc_color_array, copy=True)
                updated_bc_color_array[mask] = [255,255,255,255] # white.
                self.bc_color_array = updated_bc_color_array
        
        self.update()
    
    def toggleDensityFilter(self, p1p2=None):
        
        """
        toggles the density filter on and off.
        
        :parm str cmap:
        """
        
        status = self._filter_on
        updated_status = self._filter_on = not status # update status.
        
        # filter is on.
        if updated_status is True:
            
            if p1p2 is None: vmin, vmax = self._z.min(), self._z.max()
            else: 
                p1, p2 = p1p2
                vmin = np.percentile(self._z, p1)
                vmax = np.percentile(self._z, p2)
            c_norm = mpl.colors.Normalize(vmin, vmax)
            cmap = str(self.mainWindow.cb_cmap.currentText())
            scalarMap = mpl.pylab.cm.ScalarMappable(norm=c_norm, cmap=cmap)
            self._colors = scalarMap.to_rgba(self._z) * 255
        
        # filter is off.    
        else:
            assert self._filter_on is False # double-check; why not.
            self._colors = np.tile(np.array([0, 0, 0, 255]), (self.vertex_array.shape[0], 1))
            
        self.colorClusters()
        
class mplCanvasWidget(QWidget):
    
    """
    """
    
    def __init__(self, parent=None):
        super(mplCanvasWidget, self).__init__(parent)
        self.initUI()
        #timer = QTimer(self)
        #timer.timeout.connect(self.updateCanvas)
        #timer.start(1000)
        
    def initUI(self):
        self.figure = Figure()
        self.canvas = FigureCanvas(self.figure)
        self.toolbar = NavigationToolbar(self.canvas, self)
        self.layout = QVBoxLayout()
        self.layout.addWidget(self.toolbar)
        self.layout.addWidget(self.canvas)
        self.setLayout(self.layout)
        
    def updateCanvas(self):
        self.canvas.draw()
        
class QIPythonWidget(RichJupyterWidget):
    
    """
    Convenience class for a live IPython console widget. We can
    replace the standard banner using the customBanner argument
    
    credit: https://stackoverflow.com/questions/11513132/embedding-ipython-qt-console-in-a-pyqt-application
    """
    def __init__(self,customBanner=None,*args,**kwargs):
        
        # os.environ['QT_API'] = 'pyqt'
        # sip.setapi("QString", 2)
        # sip.setapi("QVariant", 2)
        
        if not customBanner is None: self.banner=customBanner
        super(QIPythonWidget, self).__init__(*args,**kwargs)
        
        self.kernel_manager = kernel_manager = QtInProcessKernelManager()
        kernel_manager.start_kernel()
        kernel_manager.kernel.gui = 'qt4'
        kernel_manager.kernel.shell.banner1 = ''
        
        self.kernel_client = kernel_client = self._kernel_manager.client()
        kernel_client.start_channels()

        def stop():
            kernel_client.stop_channels()
            kernel_manager.shutdown_kernel()
            guisupport.get_app_qt4().exit() 
                       
        self.exit_requested.connect(stop)

    def pushVariables(self,variableDict):
        """ Given a dictionary containing name / value pairs, push those variables to the IPython console widget """
        self.kernel_manager.kernel.shell.push(variableDict)
        
    def clearTerminal(self):
        """ Clears the terminal """
        self._control.clear()    
        
    def printText(self,text):
        """ Prints some plain text to the console """
        self._append_plain_text(text)    
            
    def executeCommand(self,command):
        """ Execute a command in the frame of the console widget """
        self._execute(command,False)
        
class QIPythonDockWidget(QMainWindow):
    
    """
    """
    
    def __init__(self, parent = None):
        super(QIPythonDockWidget, self).__init__(parent)
        
        qdock = QDockWidget()
        qdock.DockWidgetClosable = False
        titleBar = QLabel('QIPythonDockWidget')
        titleBar.setFixedHeight(20)
        qdock.setTitleBarWidget(titleBar)
        self.iPythonWidget = iPythonWidget = QIPythonWidget()
        default_banner = self.iPythonWidget.banner
        banner = default_banner + '\nexecute "who" or "whos" to list all objects in namespace.'
        self.iPythonWidget.banner = banner
        qdock.setWidget(iPythonWidget)
        self.addDockWidget(Qt.LeftDockWidgetArea, qdock)
        
        appStyle="""
        QMainWindow{
        border-color: black;
        }
        """
        self.setStyleSheet(appStyle)
        
class QTabWidgetClosable(QTabWidget):
    
    """
    a QTabWidget with closable tabs.
    
    credit - https://stackoverflow.com/questions/30840701/pyqt-qtabwidget-hide-and-close-certain-tab
    """
    
    def __init__ (self, parent = None):
        super(QTabWidgetClosable, self).__init__(parent)
        self.setTabsClosable(True)
        self.tabCloseRequested.connect(self.closeTab)

    def closeTab (self, currentIndex):
        currentQWidget = self.widget(currentIndex)
        currentQWidget.deleteLater()
        self.removeTab(currentIndex)
        
class QFingerTabBarWidget(QTabBar):
    
    """
    """
    
    def __init__(self, parent=None, *args, **kwargs):
        self.tabSize = QSize(kwargs.pop('width',100), kwargs.pop('height',25))
        QTabBar.__init__(self, parent, *args, **kwargs)
                 
    def paintEvent(self, event):
        painter = QStylePainter(self)
        option = QStyleOptionTab()
 
        for index in range(self.count()):
            self.initStyleOption(option, index)
            tabRect = self.tabRect(index)
            tabRect.moveLeft(10)
            painter.drawControl(QStyle.CE_TabBarTabShape, option)
            painter.drawText(tabRect, Qt.AlignVCenter |\
                             Qt.TextDontClip, \
                             self.tabText(index));
        painter.end()
    def tabSizeHint(self,index):
        return self.tabSize

class QFingerTabWidget(QTabWidget):
    
    """
    A QTabWidget equivalent which uses our FingerTabBarWidget
    
    credit - https://gist.github.com/LegoStormtroopr/5075267#file-fingertabs-py-L9
    """
    
    def __init__(self, parent=None, *args):
        super(QFingerTabWidget, self).__init__(parent)
        self.setTabBar(QFingerTabBarWidget(self))