from __future__ import division
import numpy as np
import math
from scipy.spatial import Delaunay
from fastkde import fastKDE as fkde
from shapely import geometry
from shapely import ops
import matplotlib.pylab as plt
from utils import _euclidean_distance

def _get_cluster_window(X, cluster_mask, buffer_ratio=0.1):
    
    """
    returns the xy coordinate points for a window of the state-
    space with a width and height as specified by the minimum and
    maximum x and y values of the cluster.
    """
    
    cluster = X[cluster_mask]
    Xx, Xy = X[:,0], X[:,1]
    cx, cy = cluster[:,0], cluster[:,1]

    # define window boundaries.
    left, right = cx.min(), cx.max()
    bottom, top = cy.min(), cy.max()
    buffer_x = abs(right-left) * buffer_ratio
    buffer_y = abs(top-bottom) * buffer_ratio
    
    # find points which fall in the window.
    window_xind = np.where((Xx > left - buffer_x) & (Xx < right + buffer_x))
    window_yind = np.where((Xy > bottom - buffer_y) & (Xy < top + buffer_y))
    
    wx = np.hstack([Xx[window_xind].reshape(-1,1), \
                    Xy[window_xind].reshape(-1,1)])
    wy = np.hstack([Xx[window_yind].reshape(-1,1), \
                    Xy[window_yind].reshape(-1,1)])
    
    intersection_ofx = np.intersect1d(wx[:,0], wy[:,0])
    match = wx[np.any(wx[:,0] == intersection_ofx[:,None], axis=0)]
    
    return match

def _get_cluster_window_mask(X, window, array_shape):
    """
    returns a mask for the points within a given window of the
    state-space (usually a window which contains the cut-out of
    a cluster).
    """
    
    window_mask = np.full(array_shape, False)
    for pt in window:
        pt_mask = np.all(X == pt, axis=1)
        idx = np.where(pt_mask==True)
        window_mask[idx] = True
        
    return window_mask

def _get_polygon_mask(X, cluster_mask, polygon):
    
    polygon_mask = np.full(cluster_mask.shape, False)
    idx = np.where(cluster_mask==True)[0]
    
    # (below) - doesn't work for some reason??
    #points = np.array([geo.Point(X[i]) for i in idx])
    points = []
    for i in idx:
        pt = geometry.Point(X[i])
        points.append(pt)
        
    for i, pt in enumerate(points):
        if pt.within(polygon):
            polygon_mask[idx[i]] = True
            
    return polygon_mask
    
def _get_best_contour_level_for_cluster(X, \
                                        cluster_mask, \
                                        closest_to=0.95, \
                                        n_levels=25,
                                        return_vertices=False):
    
    """
    returns the vertices of the contour level which best captures
    a specific percentage of the points in a cluster cut-out.
    """
    
    # TODO: speed this up :/
    
    cluster = X[cluster_mask]
    cluster_points_geo = list(map(geometry.Point, cluster))
    xy = _get_cluster_window(X, cluster_mask)
        
    x, y = xy[:,0], xy[:,1]
    pdf, axes = fkde.pdf(x, y)
    v1, v2 = axes
    levels = np.linspace(pdf.min(), pdf.max(), n_levels)
    CS = plt.contour(v1, v2, pdf, levels=levels, alpha=0)
    percent_data_captured = []

    for set in CS.allsegs:
        
        if len(set) > 1:
            percent_data_captured.append(np.nan)
            
        else:
            n=0
            level = set[0]
            polygon = geometry.Polygon(level)
            
            for pt in cluster_points_geo:
                flag = polygon.contains(pt)
                
                if flag == True:
                    n=n+1
            
            percent = n / cluster_mask.sum()
            percent_data_captured.append(percent)
            
    percent_data_captured = np.array(percent_data_captured)
    idx = np.nanargmin(np.abs(percent_data_captured-  closest_to))
    best_level = CS.allsegs[idx][0] # 0-index to return the object instead of a list
    
    if return_vertices:
        return geometry.Polygon(best_level), best_level
    
    else:
        return geometry.Polygon(best_level)
    
def _add_edge(edges, edge_points, points, i, j):
    
        """
        Add a line between the i-th and j-th points,
        if not in the list already
        """
        
        if (i, j) in edges or (j, i) in edges:
            # already added
            return
        edges.add((i, j))
        edge_points.append(points[[i, j]])
        
def _alpha_shape(points, alpha):
    
    """
    Compute the alpha shape (concave hull) of a set of points.
    
    :param numpy.ndarray points: iterable container of points.
    :param float alpha: alpha value to influence the
        gooeyness of the border. Smaller numbers
        don't fall inward as much as larger numbers.
        Too large, and you lose everything!
        
    credit: http://blog.thehumangeo.com/2014/05/12/drawing-boundaries-in-python/
    """
    
    if len(points) < 4:
        # When you have a triangle, there is no sense
        # in computing an alpha shape.
        return geometry.MultiPoint(list(points)).convex_hull
        
    tri = Delaunay(points)
    edges = set()
    edge_points = []
    
    # loop over triangles:
    # ia, ib, ic = indices of corner points of the triangle.
    for ia, ib, ic in tri.vertices:
        pa = points[ia]
        pb = points[ib]
        pc = points[ic]
        
        # Lengths of sides of triangle
        a = math.sqrt((pa[0]-pb[0])**2 + (pa[1]-pb[1])**2)
        b = math.sqrt((pb[0]-pc[0])**2 + (pb[1]-pc[1])**2)
        c = math.sqrt((pc[0]-pa[0])**2 + (pc[1]-pa[1])**2)
        
        # Semiperimeter of triangle
        s = (a + b + c)/2.0
        
        # Area of triangle by Heron's formula
        area = math.sqrt(s*(s-a)*(s-b)*(s-c))
        circum_r = a*b*c/(4.0*area)
        
        # Here's the radius filter.
        if circum_r < 1.0/alpha:
            _add_edge(edges, edge_points, points, ia, ib)
            _add_edge(edges, edge_points, points, ib, ic)
            _add_edge(edges, edge_points, points, ic, ia)
            
    m = geometry.MultiLineString(edge_points)
    triangles = list(ops.polygonize(m))
    return ops.cascaded_union(triangles), edge_points

def _get_concave_hull(xy, cluster_mask, return_vertices=False):
    
    """
    returns a polygon object and the vertices of the polygon
    for a given cluster.
    
    :param numpy.ndarray X: all xy coords.
    :param numpy.ndarray cluster_mask: indices for a target cluster.
    :param bool return_vertices: if True, returns the coords. of the
        polygon's vertices.
    :return: polygon (and coords. of the polygon's vertices)
    :rtype: shapely.geometry.polygon.Polygon
    """
    
    coords = xy[cluster_mask]
    polygon, edge_points = _alpha_shape(coords, alpha=0.4)
    vertices = np.array(polygon.exterior.coords.xy)
    
    # old method for determining buffer.
    # xy = np.array(polygon.exterior.coords.xy)
    # x, y = xy[0,:], xy[1,:]
    # x_min, x_max = x.min(), x.max()
    # y_min, y_max = y.min(), y.max()
    # x_range, y_range = abs(x_max - x_min), abs(y_max - y_min)
    # buffer = (x_range * buffer_ratio + y_range * buffer_ratio) / 2
    
    buffer_dist = _get_buffer(xy, cluster_mask, vertices)
    polygon_buffered = polygon.buffer(buffer_dist)
    xy_buffered = np.array(polygon_buffered.exterior.coords.xy).T
    
    if return_vertices:
        return polygon_buffered, xy_buffered
    
    else:
        return polygon_buffered
    
def _get_buffer(X, cluster_mask, vertices, ratio=0.1):
    
    """
    returns a percentage of the average distance from all vertices
    to the cluster's centroid.
    
    :param numpy.ndarray X: all xy coords.
    :param numpy.ndarray cluster_mask: indices for a target cluster.
    :param numpy.ndarray vertices: xy coords. for the vertices of a polygon
    :param float ratio: percentage of average distance to centroid.
    :return: buffer value
    :rtype: float
    """
    
    cluster = X[cluster_mask]
    centroid = cluster.mean(0)
    dist = np.array([_euclidean_distance(centroid, pt) for pt in vertices])
    radius = np.mean(dist)
    buffer_distance = radius * ratio
    
    return buffer_distance

def _yield_polygons(X, cluster_masks, return_vertices=False, mode='Delaunay'):
    
    """
    given the xy-coords and the cluster masks, determines the boundary
    for each cluster.
    
    :param numpy.ndarray X: all xy coordinates.
    :param list cluster_masks: list containing the cluster mask arrays.
    :param bool return_vertices: if True will return the vertices of the polygon in addition to the polygon itself.
    :param str mode: method for generating the polygons - either 'contour' or 'Delaunay'
    :return: polygon (and the vertices for the polygon if return_vertices==True)
    :rtype: shapely.geometry.polygon.Polygon
    """
    
    for mask in cluster_masks:
        if mode == 'contour':
            polygon, vertices = _get_best_contour_level_for_cluster(X, mask, return_vertices=True)
            
        elif mode == 'Delaunay':
            polygon, vertices = _get_concave_hull(X, mask, return_vertices=True)
            
        if return_vertices == False:
            yield polygon
        else:
            yield polygon, vertices

def _get_raw_score(X, cluster_masks, return_vertices=False):        
        
    """
    returns a score of the recording based on the position of any
    given point in the state-space. boundaries of the clusters are
    determined by fitting a contour line to the data in the user-
    sorted (cut) clusters.
    """
    
    # TODO: speed this up :/
    
    polygons = []
    boundaries = []
    polygon_generator = _yield_polygons(X,
                                        cluster_masks,
                                        return_vertices=True)
    
    for polygon, boundary in polygon_generator:
        polygons.append(polygon)
        boundaries.append(boundary)
    
    new_cluster_masks = []    
    for i in range(len(polygons)):
        # NOTE: use the mask for the window, not the cluster mask itself.
        mask = cluster_masks[i]
        window = _get_cluster_window(X, mask)
        window_mask = _get_cluster_window_mask(X, window, mask.shape)
        polygon = polygons[i]
        new_mask = _get_polygon_mask(X, window_mask, polygon)
        new_cluster_masks.append(new_mask)
    
    raw_score = np.full(mask.shape, -1)
    for score, new_mask in enumerate(new_cluster_masks):
        idx = np.where(new_mask==True)
        raw_score[idx] = score
        
    if return_vertices == False:
        return raw_score
    
    else:
        return raw_score, boundaries