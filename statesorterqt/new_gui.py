from __future__ import division

# very important this comes before any import from 
# PyQt4. imports from PyQt4 set the api version to
# 1 for all the classes (e.g. QString and QVariant).
import os, sip
os.environ['QT_API'] = 'pyqt'
sip.setapi("QString", 2)
sip.setapi("QVariant", 2)

from statesorterqt.widgets import glStateSorterWidget, \
    mplCanvasWidget, \
    QIPythonDockWidget, \
    QFingerTabBarWidget
    
from statesorterqt.clustr import _get_raw_score
from statesorterqt.spectr import _get_smooth_score

from statesorterqt.utils import \
    _connect_action, \
    _process_ns1, \
    _get_ctime, \
    _run_gkde, \
    _get_velocity

from PyQt4.QtGui import *
from PyQt4.QtCore import *
import numpy as np
import sys, os

class MainWindow(QMainWindow):
    
    """
    main window class.
    """
    
    def __init__(self):
        super(MainWindow, self).__init__()
        self.initUI()
    
    def initUI(self):
        
        self.mainWindow = QWidget()
        self.mainWindowLayout = QHBoxLayout()
        self.setContext()
        
        self.stateSorter = glStateSorterWidget()
        self.mplCanvas = mplCanvasWidget()
        
        self.rightPanel = QWidget()
        self.buildrightPanelLayout()
        self.rightPanel.setLayout(self.rightPanelLayout)
        self.rightPanel.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        
        self.mainTabWidget = QTabWidget()
        self.mainTabWidget.addTab(self.stateSorter, 'state sorter')
        self.mainTabWidget.addTab(self.mplCanvas, 'mpl canvas')
        
        splitter = QSplitter(Qt.Horizontal)
        splitter.addWidget(self.mainTabWidget)
        splitter.addWidget(self.rightPanel)
        splitter.setSizes([600,600])
        
        self.mainWindowLayout.addWidget(splitter)
        self.mainWindow.setLayout(self.mainWindowLayout)
        self.setCentralWidget(self.mainWindow)
        self.setGeometry(100,100,1200,600)
        
        self.sb = self.statusBar()
        self.sb.showMessage('status: ready.')
        
    def setContext(self):
        
        """
        sets the namespace for the app.
        """
        
        self._fid = None
        self._ctime = None
        self._xy = None
        self._n_channels = None
        self._z = None
        self._colors = None
        self._colormaps = ['jet', 'plasma', 'spring', 'ocean', 'cool', 'seismic', 'brg', 'gnuplot', 'gnuplot2']
        self._raw_score = None
        self._smooth_score = None
        self._boundary_polygons = None
        self._boundary_vertices = None
        self._state_ids_dict = None
        self._sorting_processed = False
        self._interaction_locked = False
        
    def buildrightPanelLayout(self):
        
        """
        builds layout for the right panel widget.
        
        layout assigned to 'self.rightPanelLayout'
        """
        
        def addToolbar():
            
            """
            adds the toolbar to the right panel's vertical layout.
            """
            row = QHBoxLayout()
            toolbar = QToolBar()
            
            openFileAction = QAction(QIcon('../icons/open_icon.png'), 'open ns1 file. Ctrl+O', self)
            openFileAction.setShortcut('Ctrl+O')
            _connect_action(openFileAction, self.openNS1)
            toolbar.addAction(openFileAction)
            
            saveSortingAction = QAction(QIcon('../icons/save_icon.png'), 'save score to csv.', self)
            saveSortingAction.triggered.connect(self.saveScoring)
            saveSortingAction.setShortcut('Ctrl+S')
            toolbar.addAction(saveSortingAction)
            
            undoCutAction = QAction(QIcon('../icons/undo_icon.png'), 'undo last cut. Ctrl+Z', self)
            undoCutAction.triggered.connect(self.stateSorter.undoCut)
            undoCutAction.setShortcut('Ctrl+Z')
            toolbar.addAction(undoCutAction)
            
            reCenterAction = QAction(QIcon('../icons/recenter_icon.png'), 're-center canvas. Ctrl+1', self)
            reCenterAction.triggered.connect(self.stateSorter.reCenterCanvas)
            reCenterAction.setShortcut('Ctrl+1')
            toolbar.addAction(reCenterAction)
            
            row.addWidget(toolbar)
            row.addStretch(1)
            self.rightPanelLayout.addLayout(row)
            
        def addControllerTabWidget():
            
            """
            """
            
            self.controllerTabWidget = QTabWidget()
            self.controllerTabWidget.setTabBar(QFingerTabBarWidget())
            self.controllerTabWidget.setTabPosition(QTabWidget.West)
            self.controllerTabWidget.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding))
            
            from tab_builds import addMainControllerTab
            addMainControllerTab(self)
            
            self.rightPanelLayout.addWidget(self.controllerTabWidget)
            
        def addIPythonWidget():
            
            """
            adds an embedded jupyter console widget to the right panel's
            vertical layout.
            """
            
            self.iPythonWidget = iPythonWidget = QIPythonDockWidget()
            self.iPythonWidget.setFixedHeight(300)
            self.rightPanelLayout.addWidget(iPythonWidget)
            
        def addLineSeparator():
            
            """
            adds a line separator to the right panel's vertical layout.
            """
            
            sepLine = QFrame()
            sepLine.setFrameShape(QFrame.HLine)
            self.rightPanelLayout.addWidget(sepLine)
        
        self.rightPanelLayout = QVBoxLayout()
        # spacer = QSpacerItem(600, 12, QSizePolicy.Expanding, QSizePolicy.Fixed)
        # self.rightPanelLayout.addSpacerItem(spacer)
        addIPythonWidget()
        addLineSeparator()
        addToolbar()
        addLineSeparator()
        addControllerTabWidget()
        self.rightPanelLayout.addStretch()
        
    #def replaceController(self, controller=None, test=False):
    #    
    #    """
    #    replaces the controller with a new layout.
    #    
    #    :param controller: a QBoxLayout or QWidget which replaces
    #        the old controller.
    #    :param bool test: if True will use a dummy widget as the
    #        new controller
    #        
    #    the controller is a layout (normally) of controls which
    #    are used to manipulate parameters of the plotting functions
    #    and some of the state-sorter's functions.
    #    """
    #    
    #    if test: controller = QLabel('this is a test!')
    #    layout = self.rightPanelLayout
    #    index = layout.count() - 1
    #            
    #    while index >= 0:
    #        
    #        child = layout.itemAt(index)
    #        if child.widget() is self.controller:
    #            _remove_child(layout, child.widget())
    #            layout.insertWidget(index, controller)
    #            self.controller = controller
    #            
    #        index -= 1
        
    def openNS1(self):
        
        """
        open file, perform signal processing, and load data into state-sorter.
        """
        
        #
        filePath = QFileDialog.getOpenFileName(self, 'H5 File', './data/', '*.ns1')  
        self._fid = str(filePath)
        self._ctime = _get_ctime(self._fid)
        
        #
        xy, all_spectra = _process_ns1(self._fid, return_spectra=True)
        self._xy = xy
        self._all_spectra = all_spectra
        self._n_channels = len(all_spectra)
        
        # initialize the stateSorter.
        self.stateSorter.initUI()
        self.stateSorter.vertex_array = xy # very important these two lines come before ...
        self.stateSorter._z = self._z = _run_gkde(xy)
        self.stateSorter.updateUIPostProcessing(self) # ... this line. you're welcome.
        
        # push objects to IPython widget.
        self.iPythonWidget.iPythonWidget.pushVariables({'fid': self._fid,
                                                        'ctime': self._ctime,
                                                        'xy': xy,
                                                        'z': self._z,
                                                        'all_spectra': self._all_spectra,
                                                        'n_channels': self._n_channels,
                                                        'figure': self.mplCanvas.figure,
                                                        'stateSorter': self.stateSorter,
                                                        'mplCanvas': self.mplCanvas})
        
        # update status bar.
        self.mainTabWidget.setCurrentIndex(0)
        filename = os.path.basename(str(filePath))
        self.statusBar().showMessage('showing: {}'.format(filename))
        self.update()
        
    def processStateSorting(self):
        
        """
        """
        
        # user defined state IDs.
        color_to_number_dict = {'blue': 0, 'orange': 1, 'green': 2}
        self._state_ids_dict = {'wake': color_to_number_dict[str(self.cb_wake.currentText())],
                                'nrem': color_to_number_dict[str(self.cb_nrem.currentText())],
                                'rem': color_to_number_dict[str(self.cb_rem.currentText())]}
    
        # get the cluster boundaries, raw scoring, and smooth scoring.            
        cluster_masks_raw = self.stateSorter._cluster_masks_raw
        raw_score, boundaries = _get_raw_score(self._xy, cluster_masks_raw, return_vertices=True)
        smooth_score = _get_smooth_score(raw_score)
        self._vel = _get_velocity(self._xy)
        self._boundary_vertices = boundaries
        self._raw_score = raw_score
        self._smooth_score = smooth_score
        
        # set flag.
        self._sorting_processed = True
        
        # define the masks for the smoothed scoring.
        self.stateSorter._cluster_masks_smooth = []
        unique_scores = np.unique(smooth_score)
        unique_scores = unique_scores[unique_scores!=-1]
        
        for score in unique_scores:
            idx = np.where(smooth_score==score)[0]
            shape = cluster_masks_raw[0].shape   
            tmp_arr = np.full(shape, False)
            tmp_arr[idx] = True
            self.stateSorter._cluster_masks_smooth.append(tmp_arr)
        
        # update the canvas.
        self.stateSorter.colorClusters(scoring_smoothed=True)
        self.stateSorter._lock_interaction = True # lock interaction after sorting is processed.
        
        # add some of the built-in plotting functionality.
        from tab_builds import addScatterPlotControllerTab
        addScatterPlotControllerTab(self)
        from tab_builds import addSpectrogramControllerTab
        addSpectrogramControllerTab(self)
        from tab_builds import addHypnogramTab
        addHypnogramTab(self)
        from tab_builds import addStateTransitionsTab
        addStateTransitionsTab(self)
        
        # push some stuff to the IPython console.
        self.iPythonWidget.iPythonWidget.pushVariables({'raw_score': self._raw_score,
                                                        'smooth_score': self._smooth_score,
                                                        'boundary_vertices': self._boundary_vertices,
                                                        'boundary_polygons': self._boundary_polygons,
                                                        'velocity': self._vel})
        
        # update status bar.
        self.statusBar().showMessage('processing complete! showing smoothed scoring. interaction locked.')
        self.mainTabWidget.setCurrentIndex(0)
        
    def saveScoring(self):
        
        """
        """
        
        self.checkContext()
        
        fid = QFileDialog.getSaveFileName(self)
        fid = str(fid)
        
        if fid.endswith('.csv') != True:
            fid = str(fid)+'.csv'
        
        with open(fid, 'w') as save_file:
            np.savetxt(save_file, self._smooth_score, fmt='%i', delimiter=',')
        
    def checkContext(self):
        
        """
        checks if the sorting is complete and processed; will halt
        execution if either is incomplete.
        """
        
        n_clusters_sorted = len(self.stateSorter._cluster_masks_raw)
        if n_clusters_sorted < 3:
            msg = 'sorting incomplete. {} clusters sorted. please sort 3.'.format(n_clusters_sorted)
            warning = QMessageBox()
            warning.setText(msg)
            warning.exec_()
            raise KeyboardInterrupt # TODO: define a custom error class for this.
        
        if self._sorting_processed is False:
            msg = 'sorting not yet processed. please process the sorting.'
            warning = QMessageBox()
            warning.setText(msg)
            warning.exec_()
            raise KeyboardInterrupt
        
if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())