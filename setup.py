from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

reqs = ['numpy',   
        'scipy ',  
        'sklearn',  
        'math',    
        'os',   
        'sys',   
        'datetime',   
        'time',   
        'warnings',    
        'matplotlib',   
        'pandas',       
        'qrangeslider',    
        'pyopengl',
        'shapely',  
        'fastkde',
        'pyopengl']

setup(
    name='statesorterqt',
    version='1.0.0',
    py_modules=['statesorterqt'],
    python_requires='>=2.7',
    install_requires=reqs)