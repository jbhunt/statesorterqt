# Description.
This is a GUI based on PyQt4 and OpenGL which allows a user to semi-manually classify time (each second of an EEG recording) into one of three states of vigilance: wakefulness, NREM sleep, or REM sleep. Recordings must be obtained with Blackrock Microsystems and recorded at a sampling rate of 500 S/s (.ns1). The vigilance state-sorting is based on methods documented in:

[Gervasoni, D., Lin, S.-C., Ribeiro, S., Soares, E. S., Pantoja, J., & Nicolelis, M. A. L. (2004). Global forebrain dynamics predict rat behavioral states and their transitions. The Journal of Neuroscience: The Official Journal of the Society for Neuroscience, 24(49),11137–11147.](http://www.jneurosci.org/content/24/49/11137.short)

# Signal processing and construction of 2D state-space.
<img src="/images/signal_processing.jpg" width="650" height="400">    

The two-part smoothing of the two first principal compenents for each set of ratios is performed using a 10" window (hanning); the authors of Gervasoni & Lin (2004) used a 20" window.

# Identification of vigilance states.  
<img src="/images/statespace.jpg" width="650" height="400">    

**B**. Relative position and shape of the different states of vigilance (note the absolute position of the clusters can change, but their position relative to each other is constant). **D**. stereotypical trajectory of state transitions.

# User interface demo.      

Open a terminal or command line and navigate to the directory which contains the statesorterqt package modules. Execute the following command:
```bash
python new_gui.py
```

<img src="/images/mainwindow_ui.png" width="750" height="400">   

Sorting can be guided/assisted by visualizing point density. The density filter can be toggled on or off with Ctrl(or Command)+f


<img src="/images/mainwindow_ui1.png" width="750" height="400">     

The user draws the estimated boundaries of the three clusters corresponding to wake, nrem sleep, and rem sleep. Those cuts are then processed by the state-sorter: the geometry of the the cut is used as a basis for creating a polygon object, the boundary of which reflects the boundary of the state in the state-space. The recording is scored second-by-second based on each point's position relative to the state boundaries. Trajectories which enter a cluster, briefly exit, and re-enter are scored with the identity of the state of origin.   

# Figure gallery - descriptions below.

|**A.**|**B.**|**C.**|
|---|---|---|
|<img src="/images/wake2nrem.png" width="200" height="150">|<img src="/images/nrem2rem.png" width="200" height="150">|<img src="/images/rem2wake.png" width="200" height="150">|

|**D.**|**E.**|**F.**|
|---|---|---|
|<img src="/images/scatterplot_density.png" width="200" height="150">|<img src="/images/scatterplot_delta.png" width="200" height="150">|<img src="/images/scatterplot_gamma.png" width="200" height="150">|    

|**G.**|**H.**|**I.**|
|---|---|---|
|<img src="/images/spectrogram.png" width="200" height="150">|<img src="/images/hypnogram.png" width="200" height="150">|<img src="/images/velocity.png" width="200" height="150">

**A.** trajectories bridging two state boundaries are considered valid state transitions if the trajectory spent atleast 15" prior to exiting the home state within the home state, 15" following entrance into the destination state within the destination state, and the total duration of the time spend in transit is less than 60". Showing transitions from wake to nrem. **B.** valid state transitions from nrem to rem sleep. **C.** valid state transitions from rem sleep back to wake. **D.** scatterplot showing point-density mapping. **E.** scatterplot showing delta-power mapping. notice delta power coincides with the nrem sleep cluster. **F.** scatterplot showing gamma-power mapping. notice gamma power coincides with the wake cluster. **G.** spectrogram (amplitude/magnitude) shown in contrast to the state-scoring (below). fs=500 samples/s, window=2" (hanning), overlap=1", resolution=0.5Hzx1". **H.** the hypnogram is a visual representation of position in the state-space over time. shown above are 9-minute blocks (rows) of time, stacked. read left-to-right, top-to-bottom is chronological sequence. color indicates position in the state-space: orange=wake, blue=nrem, green=rem, grey=inter-state. **I** Velocity through state-space mapped onto 1000" of an example recording. 